Create database adopta;

CREATE TABLE mascota
(
    identificador serial primary key,
    color text NOT NULL,
    tamanno text NOT NULL,
    sexo text NOT NULL,
    edad numeric NOT NULL,
    estado boolean NOT NULL,		
    fecha_ingreso date NOT NULL,
    foto text NOT NULL
);

CREATE TABLE duenno
(
    identificador serial primary key,
    cedula text NOT NULL,
    nombre text NOT NULL
);

CREATE TABLE duennomascota
(
     cedula text NOT NULL,
     identificador integer NOT NULL,
     fecha_adopcion date NOT NULL
);


alter table duennomascota
  add constraint FK_identificador_mascota
  foreign key (identificador)
  references mascota (identificador);

INSERT INTO public.mascota(
	 color, tamanno, sexo, edad, estado,fecha_ingreso, foto)
	VALUES ('Café', 'Grande','Macho','3','true','21-05-2020','Img\perroptb.jpg');


INSERT INTO public.mascota(
	 color, tamanno, sexo, edad, estado,fecha_ingreso, foto)
	VALUES ('Negro', 'Mediano','Macho','5','false','22-05-2020','Img\perropg.jpg');


INSERT INTO public.mascota(
	 color, tamanno, sexo, edad, estado,fecha_ingreso, foto)
	VALUES ('Gris', 'Pequeño','Macho','1','true','21-03-2020','Img\perrobt.jpg');


INSERT INTO public.mascota(
	 color, tamanno, sexo, edad, estado,fecha_ingreso, foto)
	VALUES ('Blanco', 'Mediano','Macho','2','true','21-06-2020','Img\perrortw.jpg');

INSERT INTO public.mascota(
	 color, tamanno, sexo, edad, estado,fecha_ingreso, foto)
	VALUES ('Negro', 'Mediano','Macho','4','true','13-01-2020','Img\perrortw.jpg');

	