﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace CapaDatos
{
    public class Conexion
    {

        public NpgsqlConnection conexion;


        public NpgsqlConnection ConexionBD()
        {
            try
            {
                string servidor = "localhost";
                int puerto = 5432;
                string usuario = "postgres";
                int clave = 123456;
                string baseDatos = "adopta";

                string cadenaConexion = "Server=" + servidor + ";" + "Port=" + puerto + ";" + "User Id=" + usuario + ";" + "Password=" + clave + ";" + "Database=" + baseDatos;
                conexion = new NpgsqlConnection(cadenaConexion);
                conexion.Open();




            }
            catch (NpgsqlException e)
            {

                MessageBox.Show("Revise la clase ConexiónBD de la CapaDatos o a sido " + "\n error:" + e);

            }
            return conexion;
        }
    } 
}                   

            
