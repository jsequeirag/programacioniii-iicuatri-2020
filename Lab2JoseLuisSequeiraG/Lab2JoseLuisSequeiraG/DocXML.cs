﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Lab2JoseLuisSequeiraG
{
    public class DocXML
    {
        List<int> precioMenores;
        List<int> todosPrecios;

        int numeromenor = 0;

        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Menu1.xml");

        public void LeerXml(DataGridView data)
        {
            doc.Load(ruta);


            XmlNodeList listaPlatillos = doc.SelectNodes("database/Platillos/Platillo");

            XmlNode unPlatillo;

            for (int i = 0; i < listaPlatillos.Count; i++)
            {
                unPlatillo = listaPlatillos.Item(i);
                string nombre = unPlatillo.SelectSingleNode("Nombre").InnerText;
                string precio = unPlatillo.SelectSingleNode("Precio").InnerText;
                string descripcion = unPlatillo.SelectSingleNode("Descripcion").InnerText;
                string calorias = unPlatillo.SelectSingleNode("Calorias").InnerText;





                DataTable datatable = (DataTable)data.DataSource;

                DataRow datarow1 = datatable.NewRow();
                datarow1["Nombre"] = nombre;
                datarow1["Precio"] = precio;
                datarow1["Descripcion"] = descripcion;
                datarow1["Calorias"] = calorias;
                datatable.Rows.Add(datarow1);

                data.DataSource = datatable;


            }
        }


        public void reporteXml(DataGridView data)
        {
            List<int> precioMenores = new List<int>();
            List<int> todosPrecios = new List<int>();

            doc.Load(ruta);


            XmlNodeList listaPlatillos = doc.SelectNodes("database/Platillos/Platillo");

            XmlNode unPlatillo;

            for (int i = 0; i < listaPlatillos.Count; i++)
            {
                unPlatillo = listaPlatillos.Item(i);

                string cal = unPlatillo.SelectSingleNode("Calorias").InnerText;

                if (int.Parse(cal) >= 450 && int.Parse(cal) <= 700)
                {
                    Console.WriteLine(cal);

                    string pre = unPlatillo.SelectSingleNode("Precio").InnerText;

                    todosPrecios.Add(int.Parse(pre));

                    Console.WriteLine(pre);
                }





            }
            int[] resultado = todosPrecios.Distinct().OrderByDescending(i => i).ToArray();//ordena una lista ascendentemente

            int tamanno = resultado.Length;

            int precioMenor1 = resultado[tamanno - 1];

            int precioMenor2 = resultado[tamanno - 2];


            precioMenores.Add(precioMenor1);

            Console.WriteLine("precio menor" + precioMenores[0]);

            precioMenores.Add(precioMenor2);

            Console.WriteLine("precio menor" + precioMenores[1]);


            DataTable datatable = (DataTable)data.DataSource;




            for (int i = 0; i < listaPlatillos.Count; i++)
            {
                unPlatillo = listaPlatillos.Item(i);

                string pre = unPlatillo.SelectSingleNode("Precio").InnerText;
            
                if (int.Parse(pre) == precioMenor1 || int.Parse(pre) == precioMenor2)
                {
                    unPlatillo = listaPlatillos.Item(i);
                    string nombre = unPlatillo.SelectSingleNode("Nombre").InnerText;
                    string precio = unPlatillo.SelectSingleNode("Precio").InnerText;
                    string descripcion = unPlatillo.SelectSingleNode("Descripcion").InnerText;
                    string calorias = unPlatillo.SelectSingleNode("Calorias").InnerText;


                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Nombre"] = nombre;
                    datarow1["Precio"] = precio;
                    datarow1["Descripcion"] = descripcion;
                    datarow1["Calorias"] = calorias;
                    datatable.Rows.Add(datarow1);

                    data.DataSource = datatable;
                }

            }
        }
        





        public void agregarDatos(string nombre, string precio, string descripcion, string calorias)
        {
            doc.Load(ruta);

            XmlNode platillo = agregarDatos2(nombre, precio, descripcion,calorias);

            XmlNode node = doc.SelectSingleNode("database/Platillos");
            
            node.InsertAfter(platillo, node.LastChild);

            doc.Save(ruta);
        }
        public XmlNode agregarDatos2(string nombre, string precio, string descripcion, string calorias)
        {
      
            XmlNode platillo = doc.CreateElement("Platillo");

            XmlElement xnombre = doc.CreateElement("Nombre");
            xnombre.InnerText = nombre;

            platillo.AppendChild(xnombre);

            XmlElement xprecio = doc.CreateElement("Precio");
            xprecio.InnerText = precio;
            platillo.AppendChild(xprecio);

            XmlElement xdescripcion = doc.CreateElement("Descripcion");
            xdescripcion.InnerText = descripcion;
            platillo.AppendChild(xdescripcion);

            XmlElement xcalorias = doc.CreateElement("Calorias");
            xcalorias.InnerText = calorias;
            platillo.AppendChild(xcalorias);
    
            return platillo;
        }

    }
}
