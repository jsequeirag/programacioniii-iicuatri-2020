﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace xmldoc
{
   public class xmldoc
    {

        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName,"Peliculas.xml");

        
          public void crear()
          {


              doc = new XmlDocument();

              XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

              XmlNode nodo = doc.DocumentElement;
              doc.InsertBefore(xmlDeclaration, nodo);

              XmlNode elemento = doc.CreateElement("Peliculas");
              doc.AppendChild(elemento);

             doc.Save(ruta);


          } 
        public void agregarDatos2(string codigo, string titulo, string director, string interprete, string genero) {

           doc.Load(ruta);

           XmlNode pelicula= agregarDatos(codigo, titulo, director, interprete, genero);

            XmlNode node = doc.DocumentElement;

            node.InsertAfter(pelicula,node.LastChild);

            doc.Save(ruta);
        }


        public XmlNode agregarDatos(string codigo,string titulo,string  director,string interprete,string genero)
        {
            XmlNode pelicula = doc.CreateElement("Pelicula");

            XmlElement xcodigo = doc.CreateElement("Codigo");
            xcodigo.InnerText = codigo;
            pelicula.AppendChild(xcodigo);

            XmlElement xtitulo = doc.CreateElement("Titulo");
            xtitulo.InnerText = titulo;
            pelicula.AppendChild(xtitulo);

            XmlElement xdirector = doc.CreateElement("Director");
            xdirector.InnerText = director;
            pelicula.AppendChild(xdirector);

            XmlElement xinterprete = doc.CreateElement("Interprete");
            xinterprete.InnerText = interprete;
            pelicula.AppendChild(xinterprete);


            XmlElement xgenero = doc.CreateElement("Genero");
            xgenero.InnerText = genero;
            pelicula.AppendChild(xgenero);

            return pelicula;
        }


        public void LeerXml(DataGridView data)
        {
            doc.Load(ruta);


            XmlNodeList listapeliculas = doc.SelectNodes("Peliculas/Pelicula");

            XmlNode unaPelicula;

            for (int i = 0; i < listapeliculas.Count; i++)
            {
                unaPelicula = listapeliculas.Item(i);
                string codigo = unaPelicula.SelectSingleNode("Codigo").InnerText;
                string titulo = unaPelicula.SelectSingleNode("Titulo").InnerText;
                string director = unaPelicula.SelectSingleNode("Director").InnerText;
                string interprete = unaPelicula.SelectSingleNode("Interprete").InnerText;
                string genero = unaPelicula.SelectSingleNode("Genero").InnerText;

                string cadena = (codigo, titulo, director, interprete, genero).ToString();


                DataTable datatable = (DataTable)data.DataSource;

                DataRow datarow1 = datatable.NewRow();
                datarow1["codigo"] = codigo;
                datarow1["titulo"] = titulo;
                datarow1["director"] = director;
                datarow1["interprete"] = interprete;
                datarow1["genero"] = genero;
                datatable.Rows.Add(datarow1);
                data.DataSource = datatable;

            }
        }
            public void ModificarXml(string codigo, string titulo, string director, string interprete, string genero)
            {
                XmlNodeList listaPeliculas = doc.SelectNodes("Peliculas/Pelicula");

                foreach (XmlNode item in listaPeliculas)
                {
                    if (item.FirstChild.InnerText == codigo)
                    {
                        item.SelectSingleNode("Codigo").InnerText = codigo;
                        item.SelectSingleNode("Titulo").InnerText = titulo;
                        item.SelectSingleNode("Director").InnerText = director;
                        
                        item.SelectSingleNode("Interprete").InnerText = interprete;
                        item.SelectSingleNode("Genero").InnerText = genero;
                    }
                }
                doc.Save(ruta);
            }

        public void EliminarXml(string id)
        {
            doc.Load(ruta);

            XmlNodeList listaEmpleados = doc.SelectNodes("Peliculas/Pelicula");

            XmlNode borra = doc.DocumentElement;

            foreach (XmlNode item in listaEmpleados)
            {
                if (item.SelectSingleNode("Codigo").InnerText == id)
                {
                    XmlNode borrar = item;
                    borra.RemoveChild(borrar);
                }
            }

            doc.Save(ruta);
        }

    }

    }

