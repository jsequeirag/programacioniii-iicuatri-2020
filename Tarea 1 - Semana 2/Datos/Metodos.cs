﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;



namespace Datos
{
   public  class Metodos
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public void insertarDetalle(string cedula, double kilogramos, string tipo, string tamanno,double valor )
        {
            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand("INSERT INTO vc.detalle_producto (cedula, kilogramos, tipo,tamanno, valor) VALUES ('" + cedula + "', '" + kilogramos + "','" + tipo + "', '" + tamanno + "','" + valor + "')", conexionRetorno);
            cmd.ExecuteNonQuery();
        }


        public List<Object> ConsultarDatos(string cedula)
        {



            conexionRetorno = conexion.ConexionBD();

            List<Object> lista = new List<Object>();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT cedula, kilogramos, tipo, valor, tamanno from vc.detalle_producto WHERE cedula =   '{cedula}' ", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add( "cedula:"+dr.GetString(0)+ "kilogramos: " + dr.GetString(1) + " tipo: " + dr.GetString(2) + " valor: " + dr.GetString(3) + " tamanno: " + dr.GetString(4));

                }
            
            }



         
            return lista;
        }




        public double obtenerPrecio()
        {



            conexionRetorno = conexion.ConexionBD();

            double  precio = 0;
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT precio FROM vc.precio WHERE id = 1", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    precio = double.Parse(dr.GetString(0));

                }
            }
            else {

                precio = 0;
            }
      

            return precio;
        }


        public String comprobarPersona(string cedula)
        {

            conexionRetorno = conexion.ConexionBD();

            string registrado="";

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT cedula FROM vc.persona WHERE cedula = '{cedula}'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {

                  registrado = "s";

                
            }
            else
            {
                 registrado = "n";
            }
            conexionRetorno.Close();

            return registrado;
        }


    }
}
