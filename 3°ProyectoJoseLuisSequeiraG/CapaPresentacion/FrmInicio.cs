﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;


namespace CapaPresentacion
{
    public partial class FrmInicio : Form
    {
        public FrmInicio()
        {
            InitializeComponent();
            CenterToScreen();
            new CNCargarDatos().cargarDatosBD();
        }

        private void btnAcceder_Click(object sender, EventArgs e)
        {
            List<string> datosUsuario = new CNUsuario().seleccionarUsuario(tbUsuario.Text,tbContrasenna.Text);

            if (datosUsuario.Count != 0)
            {
                MessageBox.Show("Usuario registrado");
                FrmMenu frm = new FrmMenu(datosUsuario);
                frm.Visible = true;
                this.Hide();
            }

        }

        private void lbRegistro_Click(object sender, EventArgs e)
        {
            FrmRegistro frm = new FrmRegistro();
            frm.Visible=true;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
