﻿using _3_ProyectoJoseLuisSequeiraG;
using CapaNegocio;
using CapaNegocioh;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebService;

namespace CapaPresentacion
{
    public partial class FrmMenu : Form
    {
   

        List<string> datosUsuario;
        Dictionary<string, string> dic;

        public FrmMenu(List<string> datosUsuario)
        {
            InitializeComponent();
            CenterToScreen();
            this.datosUsuario = datosUsuario;
            CargarDatos();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmInicio frm = new FrmInicio();

            frm.Visible = true;

            this.Hide();

        }

        public void CargarDatos()
        {
            lbNombre.Text = datosUsuario[0];
            lbCedula.Text = datosUsuario[1];
           
            Dictionary<string, string> dic= new Dictionary<string, string>();
            
            new CNImagen().cargarurlImagen(dic);

            foreach (var datos in dic ) {

                if (datos.Key == datosUsuario[4])
                {
                    string url = datos.Value;

                    var request = WebRequest.Create(url);

                    using (var response = request.GetResponse())
                    using (var stream = response.GetResponseStream())
                    {
                        imgBandera.BackgroundImage = Bitmap.FromStream(stream);
                    }

                }


            }

        }

        private void btnBanderas_Click(object sender, EventArgs e)
        {
            FrmJuegoBanderas frm = new FrmJuegoBanderas(this,this.datosUsuario);
            frm.Visible = true;
            this.Visible = false;
        }

        private void btnPalabras_Click(object sender, EventArgs e)
        {
            FrmJuegoPalabras frm = new FrmJuegoPalabras(this, this.datosUsuario);
            frm.Visible = true;
            this.Hide();

        }

        private void btnRecord_Click(object sender, EventArgs e)
        {
            FrmRecord frm = new FrmRecord(this,datosUsuario[1]);
            frm.Visible = true;
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmRecordGlobal frm = new FrmRecordGlobal(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnRecordPersonal_Click(object sender, EventArgs e)
        {
            FrmRecordPersona frm = new FrmRecordPersona(this);
            frm.Visible = true;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmInfo frm = new FrmInfo(this);
            frm.Visible = true;
            this.Hide();
        }
    }
    }

