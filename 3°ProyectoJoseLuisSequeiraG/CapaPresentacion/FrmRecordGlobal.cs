﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmRecordGlobal : Form
    {
        FrmMenu parent;
        public FrmRecordGlobal(FrmMenu parent)
        {
            InitializeComponent();
            this.parent = parent;
            definirDgv();
            cargarDatos();
            CenterToScreen();
            establecerFilas();

        }
        public void establecerFilas()
        {

            dgvBandera.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvBandera.DefaultCellStyle.SelectionForeColor = Color.Black;
            dgvPalabra.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvPalabra.DefaultCellStyle.SelectionForeColor = Color.Black;
        }

        public void definirDgv() {
            DataGridViewImageColumn vic = new DataGridViewImageColumn();

            DataTable dataTable = new DataTable();  
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Pais");
            dataTable.Columns.Add("Puntos");
            dataTable.Columns.Add("Cedula");
            dgvBandera.DataSource = dataTable;

            DataTable dataTable1 = new DataTable();
            dataTable1.Columns.Add("Nombre");
            dataTable1.Columns.Add("Pais");
            dataTable1.Columns.Add("Puntos");
            dataTable1.Columns.Add("Cedula");
            dgvPalabra.DataSource = dataTable1;


        }
        public void cargarDatos()
        {
            new CNRecord().obtenerrecordGlobal(dgvBandera,"Bandera");
            new CNRecord().obtenerrecordGlobal(dgvPalabra, "Palabra");

        }

        private void FrmRecordGlobal_Load(object sender, EventArgs e)
        {

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.parent.Visible=true;
            this.Hide();

        }

        private void dgvBandera_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgvBandera.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgvBandera.DefaultCellStyle.SelectionForeColor = Color.White;
                FrmRecord frm = new FrmRecord(this, dgvBandera.Rows[e.RowIndex].Cells[3].Value.ToString());
                frm.Visible = true;
                this.Hide();

            }
        }

        private void dgvPalabra_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgvPalabra.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgvPalabra.DefaultCellStyle.SelectionForeColor = Color.White;
                FrmRecord frm = new FrmRecord(this, dgvPalabra.Rows[e.RowIndex].Cells[3].Value.ToString());
                frm.Visible = true;
                this.Hide();

            }
        }
    }
}
