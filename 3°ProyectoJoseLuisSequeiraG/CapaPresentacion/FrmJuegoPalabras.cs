﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.XPath;
using CapaNegocio;
using CapaNegocioh;

namespace CapaPresentacion
{
    public partial class FrmJuegoPalabras : Form
    {
        FrmMenu parent;

        int incorrrectas = 0;
        int correctas = 0;
        int puntos = 0;
        int minutos = 0;

        Stopwatch osw = new Stopwatch();

        List<string> datosUsuario;

        Dictionary<string, string> dicContinentes;
        Dictionary<string, string> dicPaises;
        Dictionary<string, string> dicurl;

        string pais;
        
        public FrmJuegoPalabras(FrmMenu parent,List<string>datosUsuario)
        {
            InitializeComponent();
            CenterToScreen();
            cargarDatosDB();
            cargarComponentes();
            this.parent = parent;
            cbContinentes.SelectedIndex = 0;
            this.datosUsuario = datosUsuario;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)osw.ElapsedMilliseconds);
            btnMinutos.Text = ts.Minutes.ToString();
            btnSegundos.Text = ts.Seconds.ToString();
            if (ts.Minutes == this.minutos && ts.Minutes != 0)
            {

                timer1.Enabled = false;
                osw.Restart();
                MessageBox.Show("Tiempo agotado!!!!");
  
                btnMinutos.Text = "0";
                btnSegundos.Text = "0";
                pJuego.Enabled = false;
                pOpciones.Enabled = true;
                btnImg.BackgroundImage = null;
                btnPuntos.Text = "";

                new CNRecord().registrarRecord(datosUsuario[1],"Palabra",this.correctas,this.incorrrectas,this.puntos,this.minutos);
                this.correctas = 0;
                this.incorrrectas = 0;
                this.puntos = 0;
                this.minutos = 0;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            rb1.Checked = false;
            rb2.Checked = false;
            rb3.Checked = false;

            try
            {

                int verificacion = int.Parse(tbMinutos.Text) * 0;


            }
            catch
            {

                MessageBox.Show("Por favor digite solo enteros!");
                tbMinutos.Text = "";
            }
        }
        private void rb3_Click(object sender, EventArgs e)
        {

            tbMinutos.Text = "";
            rb3.Checked = true;
        }

        private void rb2_Click(object sender, EventArgs e)
        {
            tbMinutos.Text = "";
            rb2.Checked = true;
        }

        private void rb1_Click(object sender, EventArgs e)
        {
            tbMinutos.Text = "";
            rb1.Checked = true;
        }

        private void btnComenzar_Click(object sender, EventArgs e)
        {
            if (rb1.Checked == true)
            {
                this.minutos = 3;
            }
            if (rb2.Checked == true)
            {
                this.minutos = 5;
            }
            if (rb3.Checked == true)
            {
                this.minutos = 7;
            }
            if (tbMinutos.Text != "")
            {

                this.minutos = int.Parse(tbMinutos.Text);

            }

            timer1.Enabled = false;
            osw.Restart();

            btnMinutos.Text = "0";
            btnSegundos.Text = "0";

            timer1.Enabled = true;
            osw.Start();

            pJuego.Enabled = true;

            pOpciones.Enabled = false;

            cargarPaises();
            cargarImagen();
        }


        public void cargarDatosDB()
        {

            new CNCargarDatos().cargarDatosBD();

        }

        public void cargarComponentes()
        {


            dicContinentes = new Dictionary<string, string>();
            new CNContinente().cargarcbContinentes(cbContinentes, dicContinentes);
            dicurl = new Dictionary<string, string>();
            new CNImagen().cargarurlImagen(dicurl);
        }

        public void cargarPaises()
        {

            string isoContinente = "";
            string continente = cbContinentes.SelectedItem.ToString();

            foreach (var c in dicContinentes)
            {

                if (c.Key == continente)
                {

                    isoContinente = c.Value;
                }
                dicPaises = new Dictionary<string, string>();

                new CNPais().cargarPaises(dicPaises, isoContinente);

            }

        }

        public void cargarImagen()
        {
            string palabraDesordenada = "";
            string pista= "";

            int numero = new Random().Next(0, (dicPaises.Count) - 1);

            var dic = dicPaises.ElementAt(numero);

            this.pais = dic.Key;

            Console.WriteLine(pais);
            List<string> palabra = new List<string>();
            Console.WriteLine(pais);
            for (int x = 0; x < pais.Length; x++) {

                string cr = pais.ElementAt(x).ToString();
                palabra.Add(cr);
               
            }

            var n = palabra.Count;
            var rnd = new Random();

            for (int i = n - 1; i > 0; i--)
            {
                var j = rnd.Next(0, i);
                var temp = palabra[i];

                if (temp != " ")
                {

                    palabra[i] = palabra[j];
                    palabra[j] = temp;

                }


            }

            for (int x = 0; x < palabra.Count; x++)
            {

                palabraDesordenada = palabraDesordenada + palabra[x];
            }
         
            btnImg.Text = palabraDesordenada;

            int cortarPalabra = (pais.Length) - 3;

            for (int x = 0; x < pais.Length; x++)
            {
                if (x > cortarPalabra)
                {
                    pista = pista + pais[x] ;

                }

                if (x == 0) {
                    pista = pista + pais[0];
                }
                if (x > 0  && x<=cortarPalabra )
                {
                    pista = pista +"_ ";
                }

            }
            btnPista.Text = pista;
        }


        private void txtPais_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtPais.Text == this.pais)
                {

                    this.puntos = this.puntos + 10;
                    btnPuntos.Text = puntos.ToString();
                    this.correctas = correctas + 1;

                }
                else {
                    this.incorrrectas = incorrrectas + 1;


                }
                cargarImagen();
                txtPais.Text = "";

            }

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.parent.Visible=true;
            this.Hide();
        }
    }
}
