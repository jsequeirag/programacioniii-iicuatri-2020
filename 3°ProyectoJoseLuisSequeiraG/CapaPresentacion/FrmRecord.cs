﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmRecord : Form
    {

        FrmMenu parentMenu;
        FrmRecordPersona parentPersonal;
        FrmRecordGlobal parentGlobal;

        string cedulaUsuario;
        List<int> listPalabra;
        List<int> listBandera;
        public FrmRecord(FrmMenu parent,string cedulaUsuario)
        {
            InitializeComponent();
            this.parentMenu = parent;
            this.cedulaUsuario = cedulaUsuario;
            CenterToParent();
            listBandera = new List<int>();
            listPalabra = new List<int>();

            new CNRecord().obtenerRecord(cedulaUsuario,"Bandera",listBandera);
            new CNRecord().obtenerRecord(cedulaUsuario, "Palabra", listPalabra);

            establecergraficoBandera();
            establecergraficoPalabra();

        }
        public FrmRecord(FrmRecordPersona parent, string cedulaUsuario)
        {
            InitializeComponent();
            this.parentPersonal = parent;
            this.cedulaUsuario = cedulaUsuario;
            CenterToParent();
            listBandera = new List<int>();
            listPalabra = new List<int>();

            new CNRecord().obtenerRecord(cedulaUsuario, "Bandera", listBandera);
            new CNRecord().obtenerRecord(cedulaUsuario, "Palabra", listPalabra);

            establecergraficoBandera();
            establecergraficoPalabra();

        }
        public FrmRecord(FrmRecordGlobal parent, string cedulaUsuario)
        {
            InitializeComponent();
            this.parentGlobal = parent;
            this.cedulaUsuario = cedulaUsuario;
            CenterToParent();
            listBandera = new List<int>();
            listPalabra = new List<int>();

            new CNRecord().obtenerRecord(cedulaUsuario, "Bandera", listBandera);
            new CNRecord().obtenerRecord(cedulaUsuario, "Palabra", listPalabra);

            establecergraficoBandera();
            establecergraficoPalabra();
        }
            public void establecergraficoBandera()
        {
            try
            {
                int porncentajecorrectas = (listBandera[0] * 100) / (listBandera[0] + listBandera[1]);
                int porncentajeIncorrectas = (listBandera[1] * 100) / (listBandera[0] + listBandera[1]);

                graficoBandera.Titles.Add($"Basado en {listBandera[0] + listBandera[1]} respuestas(MINIJUEGO BANDERAS)");

                graficoBandera.Series["Series1"].LegendText = "Respuestas correctas";

                graficoBandera.Series["Series2"].LegendText = "Respuestas incorrectas";

                graficoBandera.Series["Series1"].Points.AddXY("", porncentajecorrectas);

                graficoBandera.Series["Series2"].Points.AddXY("", porncentajeIncorrectas);

                graficoBandera.Series["Series1"].Label = $"{porncentajecorrectas}%";

                graficoBandera.Series["Series2"].Label = $"{porncentajeIncorrectas}%";

                btnbanMinutos.Text = listBandera[3].ToString();
                btnbanPuntos.Text = listBandera[2].ToString();
                btnbanPartidas.Text = listBandera[4].ToString();
            }
            catch {
                MessageBox.Show("Faltan datos para procesar(MINIJUEGO BANDERAS)");
            
            }
        }

        public void establecergraficoPalabra()
        {
            try
            {

                int porncentajecorrectas = (listPalabra[0] * 100) / (listPalabra[0] + listPalabra[1]);
                int porncentajeIncorrectas = (listPalabra[1] * 100) / (listPalabra[0] + listPalabra[1]);

                graficoPalabra.Titles.Add($"Basado en {listPalabra[0] + listPalabra[1]} respuestas(MINIJUEGO PALABRAS)");

                graficoPalabra.Series["Series1"].LegendText = "Respuestas correctas";

                graficoPalabra.Series["Series2"].LegendText = "Respuestas incorrectas";

                graficoPalabra.Series["Series1"].Points.AddXY("", porncentajecorrectas);

                graficoPalabra.Series["Series2"].Points.AddXY("", porncentajeIncorrectas);

                graficoPalabra.Series["Series1"].Label = $"{porncentajecorrectas}%";

                graficoPalabra.Series["Series2"].Label = $"{porncentajeIncorrectas}%";


                btnpalMinutos.Text = listPalabra[3].ToString();
                btnpalPuntos.Text = listPalabra[2].ToString();
                btnpalPartidas.Text = listPalabra[4].ToString();

            }
            catch
            {
                MessageBox.Show("Faltan datos para procesar(MINIJUEGO PALABRAS)");
            }
        }
        private void FrmRecord_Load(object sender, EventArgs e)
        {

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {

            if (parentMenu != null)
            {
                this.parentMenu.Visible = true;
                this.Hide();
            }
            if (parentPersonal != null)
            {
                this.parentPersonal.Visible = true;
                this.Hide();
            }
            if (parentGlobal != null)
            {
                this.parentGlobal.Visible = true;
                this.Hide();
            }
        }

 
    }
}
