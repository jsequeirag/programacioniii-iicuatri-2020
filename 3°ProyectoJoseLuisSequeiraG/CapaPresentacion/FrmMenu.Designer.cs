﻿namespace CapaPresentacion
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBanderas = new System.Windows.Forms.Button();
            this.btnPalabras = new System.Windows.Forms.Button();
            this.btnRecord = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.lbNombre = new System.Windows.Forms.Label();
            this.imgBandera = new System.Windows.Forms.Button();
            this.lbCedula = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnRecordPersonal = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBanderas
            // 
            this.btnBanderas.Location = new System.Drawing.Point(87, 120);
            this.btnBanderas.Name = "btnBanderas";
            this.btnBanderas.Size = new System.Drawing.Size(196, 23);
            this.btnBanderas.TabIndex = 0;
            this.btnBanderas.Text = "Juego de banderas";
            this.btnBanderas.UseVisualStyleBackColor = true;
            this.btnBanderas.Click += new System.EventHandler(this.btnBanderas_Click);
            // 
            // btnPalabras
            // 
            this.btnPalabras.Location = new System.Drawing.Point(87, 149);
            this.btnPalabras.Name = "btnPalabras";
            this.btnPalabras.Size = new System.Drawing.Size(196, 23);
            this.btnPalabras.TabIndex = 1;
            this.btnPalabras.Text = "Juego de palabras";
            this.btnPalabras.UseVisualStyleBackColor = true;
            this.btnPalabras.Click += new System.EventHandler(this.btnPalabras_Click);
            // 
            // btnRecord
            // 
            this.btnRecord.Location = new System.Drawing.Point(87, 178);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(196, 23);
            this.btnRecord.TabIndex = 2;
            this.btnRecord.Text = "Mi record";
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(21, 319);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Atras";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.BackColor = System.Drawing.Color.Transparent;
            this.lbNombre.Location = new System.Drawing.Point(145, 12);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(111, 17);
            this.lbNombre.TabIndex = 4;
            this.lbNombre.Text = "Nombre Usuario";
            // 
            // imgBandera
            // 
            this.imgBandera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgBandera.Location = new System.Drawing.Point(12, 12);
            this.imgBandera.Name = "imgBandera";
            this.imgBandera.Size = new System.Drawing.Size(118, 62);
            this.imgBandera.TabIndex = 5;
            this.imgBandera.UseVisualStyleBackColor = true;
            // 
            // lbCedula
            // 
            this.lbCedula.AutoSize = true;
            this.lbCedula.BackColor = System.Drawing.Color.Transparent;
            this.lbCedula.Location = new System.Drawing.Point(145, 48);
            this.lbCedula.Name = "lbCedula";
            this.lbCedula.Size = new System.Drawing.Size(52, 17);
            this.lbCedula.TabIndex = 6;
            this.lbCedula.Text = "Cedula";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(87, 207);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Records Globales";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRecordPersonal
            // 
            this.btnRecordPersonal.Location = new System.Drawing.Point(87, 236);
            this.btnRecordPersonal.Name = "btnRecordPersonal";
            this.btnRecordPersonal.Size = new System.Drawing.Size(196, 23);
            this.btnRecordPersonal.TabIndex = 8;
            this.btnRecordPersonal.Text = "Records Personales";
            this.btnRecordPersonal.UseVisualStyleBackColor = true;
            this.btnRecordPersonal.Click += new System.EventHandler(this.btnRecordPersonal_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(87, 91);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(196, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Info";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CapaPresentacion.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(364, 352);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnRecordPersonal);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbCedula);
            this.Controls.Add(this.imgBandera);
            this.Controls.Add(this.lbNombre);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnRecord);
            this.Controls.Add(this.btnPalabras);
            this.Controls.Add(this.btnBanderas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMenu";
            this.Text = "FrmMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBanderas;
        private System.Windows.Forms.Button btnPalabras;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Button imgBandera;
        private System.Windows.Forms.Label lbCedula;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRecordPersonal;
        private System.Windows.Forms.Button button2;
    }
}