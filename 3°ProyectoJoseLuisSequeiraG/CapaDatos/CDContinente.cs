﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CapaDatos
{
   public class CDContinente
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();
        public void cargarcbContinentes(ComboBox cb,Dictionary<string,string>dic)
        {
           
            conexionRetorno = conexion.ConexionBD();
            conexion.conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre,cdgoiso from ip.continente", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

          

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    cb.Items.Add(dr.GetString(0));
                    dic.Add (dr.GetString(0), dr.GetString(1));

                }

            }
            conexion.conexion.Close();
        }


    }
}
