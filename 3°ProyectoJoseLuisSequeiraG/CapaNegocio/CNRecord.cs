﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CapaDatos;
namespace CapaNegocio
{
    public class CNRecord
    {

        public void registrarRecord(string cedula, string tipo, int correctas, int incorrectas, int puntos, int tiempo)
        {

           
            new CDRecord().registrarRecord(cedula,tipo,correctas,incorrectas,puntos,tiempo);

           
        }
        public void obtenerRecord(string cedula, string tipo, List<int>datos)
        {

            new CDRecord().obtenerRecord(cedula, tipo,datos);



        }

        public void obtenerrecordGlobal(DataGridView dgv,string tipo)
        {
            new CDRecord().obtenerrecordGlobal(dgv,tipo);
        
        }
        public void obtenerrecordPersonal(DataGridView dgv)
        {
            new CDRecord().obtenerrecordPersonal(dgv);

        }
        
    }
}