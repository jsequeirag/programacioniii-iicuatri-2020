﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteUno
{
    public partial class Mensajeria : Form
    {
        private MessageBoxButtons tipoBoton;
        private MessageBoxIcon tipoIcono;

        public Mensajeria()
        {
            InitializeComponent();
            tipoBoton = MessageBoxButtons.OK;
            tipoIcono = MessageBoxIcon.Information;
        }

        private void CambiarTipo(object sender, EventArgs e)
        {
            RadioButton temp = (RadioButton)sender;
            if (temp.Checked)
            {
                if (temp == radioButton1)
                {
                    tipoBoton = MessageBoxButtons.OK;
                }
                else if (temp == radioButton2)
                {
                    tipoBoton = MessageBoxButtons.OKCancel;
                }
                else if (temp == radioButton3)
                {
                    tipoBoton = MessageBoxButtons.YesNo;
                }
                else if (temp == radioButton4)
                {
                    tipoBoton = MessageBoxButtons.YesNoCancel;
                }
                else if (temp == radioButton5)
                {
                    tipoBoton = MessageBoxButtons.RetryCancel;
                }
                else
                {
                    tipoBoton = MessageBoxButtons.AbortRetryIgnore;
                }
            }
        }

        private void CambiarIcono(object sender, EventArgs e)
        {
            RadioButton temp = (RadioButton)sender;
            if (temp.Checked)
            {
                Console.WriteLine(temp.Name);
                if (temp == radioButton10)
                {
                    tipoIcono = MessageBoxIcon.Information;
                }
                else if (temp == radioButton8)
                {
                    tipoIcono = MessageBoxIcon.Exclamation;
                }
                else if (temp == radioButton7)
                {
                    tipoIcono = MessageBoxIcon.Question;
                }
                else
                {
                    tipoIcono = MessageBoxIcon.Error;
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Mensaje a Desplegar", "Título de la Ventana", tipoBoton, tipoIcono);
            switch (res)
            {
                case DialogResult.OK:
                    label2.Text = "Seleccionó Ok";
                    break;
                case DialogResult.Cancel:
                    label2.Text = "Seleccionó Cancel";
                    break;
                case DialogResult.Yes:
                    label2.Text = "Seleccionó Yes";
                    break;
                case DialogResult.No:
                    label2.Text = "Seleccionó No";
                    break;
                case DialogResult.Ignore:
                    label2.Text = "Seleccionó Ignore";
                    break;
                case DialogResult.Abort:
                    label2.Text = "Seleccionó Abort";
                    break;
                case DialogResult.Retry:
                    label2.Text = "Seleccionó Retry";
                    break;
            }
        }

        private void Mensajeria_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null) {
                Owner.Show();
            }
        }
    }
}
