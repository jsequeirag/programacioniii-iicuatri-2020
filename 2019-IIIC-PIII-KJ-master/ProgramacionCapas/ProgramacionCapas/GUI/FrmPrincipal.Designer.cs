﻿namespace ProgramacionCapas.GUI
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.gbJugUno = new System.Windows.Forms.GroupBox();
            this.lblIntUno = new System.Windows.Forms.Label();
            this.lblPunUno = new System.Windows.Forms.Label();
            this.gbJugDos = new System.Windows.Forms.GroupBox();
            this.lblIntDos = new System.Windows.Forms.Label();
            this.lblPunDos = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnLanzar = new System.Windows.Forms.Button();
            this.txtNum = new System.Windows.Forms.TextBox();
            this.pbxDadoUno = new System.Windows.Forms.PictureBox();
            this.pbxDadoDos = new System.Windows.Forms.PictureBox();
            this.gbJugUno.SuspendLayout();
            this.gbJugDos.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDadoUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDadoDos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(751, 45);
            this.label1.TabIndex = 3;
            this.label1.Text = "DADOS - UTN v0.1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbJugUno
            // 
            this.gbJugUno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.gbJugUno.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gbJugUno.Controls.Add(this.lblIntUno);
            this.gbJugUno.Controls.Add(this.lblPunUno);
            this.gbJugUno.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbJugUno.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gbJugUno.Location = new System.Drawing.Point(11, 72);
            this.gbJugUno.Margin = new System.Windows.Forms.Padding(6);
            this.gbJugUno.Name = "gbJugUno";
            this.gbJugUno.Padding = new System.Windows.Forms.Padding(6);
            this.gbJugUno.Size = new System.Drawing.Size(354, 177);
            this.gbJugUno.TabIndex = 2;
            this.gbJugUno.TabStop = false;
            this.gbJugUno.Text = "Jugador #1";
            // 
            // lblIntUno
            // 
            this.lblIntUno.AutoSize = true;
            this.lblIntUno.Font = new System.Drawing.Font("Agency FB", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntUno.Location = new System.Drawing.Point(272, 21);
            this.lblIntUno.Name = "lblIntUno";
            this.lblIntUno.Size = new System.Drawing.Size(67, 39);
            this.lblIntUno.TabIndex = 5;
            this.lblIntUno.Text = "2/10";
            // 
            // lblPunUno
            // 
            this.lblPunUno.AutoSize = true;
            this.lblPunUno.Font = new System.Drawing.Font("Agency FB", 49.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPunUno.Location = new System.Drawing.Point(112, 42);
            this.lblPunUno.Name = "lblPunUno";
            this.lblPunUno.Size = new System.Drawing.Size(118, 100);
            this.lblPunUno.TabIndex = 4;
            this.lblPunUno.Text = "95";
            // 
            // gbJugDos
            // 
            this.gbJugDos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.gbJugDos.Controls.Add(this.lblIntDos);
            this.gbJugDos.Controls.Add(this.lblPunDos);
            this.gbJugDos.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbJugDos.Location = new System.Drawing.Point(386, 72);
            this.gbJugDos.Margin = new System.Windows.Forms.Padding(6);
            this.gbJugDos.Name = "gbJugDos";
            this.gbJugDos.Padding = new System.Windows.Forms.Padding(6);
            this.gbJugDos.Size = new System.Drawing.Size(354, 177);
            this.gbJugDos.TabIndex = 6;
            this.gbJugDos.TabStop = false;
            this.gbJugDos.Text = "Jugador #2";
            // 
            // lblIntDos
            // 
            this.lblIntDos.AutoSize = true;
            this.lblIntDos.Font = new System.Drawing.Font("Agency FB", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntDos.Location = new System.Drawing.Point(272, 21);
            this.lblIntDos.Name = "lblIntDos";
            this.lblIntDos.Size = new System.Drawing.Size(67, 39);
            this.lblIntDos.TabIndex = 5;
            this.lblIntDos.Text = "2/10";
            // 
            // lblPunDos
            // 
            this.lblPunDos.AutoSize = true;
            this.lblPunDos.Font = new System.Drawing.Font("Agency FB", 49.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPunDos.Location = new System.Drawing.Point(112, 42);
            this.lblPunDos.Name = "lblPunDos";
            this.lblPunDos.Size = new System.Drawing.Size(118, 100);
            this.lblPunDos.TabIndex = 4;
            this.lblPunDos.Text = "95";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBox3.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox3.Controls.Add(this.btnLanzar);
            this.groupBox3.Controls.Add(this.txtNum);
            this.groupBox3.Controls.Add(this.pbxDadoUno);
            this.groupBox3.Controls.Add(this.pbxDadoDos);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(11, 261);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox3.Size = new System.Drawing.Size(729, 205);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Juego";
            // 
            // btnLanzar
            // 
            this.btnLanzar.Enabled = false;
            this.btnLanzar.Location = new System.Drawing.Point(167, 76);
            this.btnLanzar.Name = "btnLanzar";
            this.btnLanzar.Size = new System.Drawing.Size(178, 79);
            this.btnLanzar.TabIndex = 3;
            this.btnLanzar.Text = "Lanzar Dados";
            this.btnLanzar.UseVisualStyleBackColor = true;
            this.btnLanzar.Click += new System.EventHandler(this.btnLanzar_Click);
            // 
            // txtNum
            // 
            this.txtNum.Font = new System.Drawing.Font("Agency FB", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNum.Location = new System.Drawing.Point(29, 76);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(100, 79);
            this.txtNum.TabIndex = 2;
            this.txtNum.TextChanged += new System.EventHandler(this.txtNum_TextChanged);
            // 
            // pbxDadoUno
            // 
            this.pbxDadoUno.Image = global::ProgramacionCapas.Properties.Resources.D1;
            this.pbxDadoUno.Location = new System.Drawing.Point(382, 32);
            this.pbxDadoUno.Name = "pbxDadoUno";
            this.pbxDadoUno.Size = new System.Drawing.Size(158, 158);
            this.pbxDadoUno.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxDadoUno.TabIndex = 1;
            this.pbxDadoUno.TabStop = false;
            // 
            // pbxDadoDos
            // 
            this.pbxDadoDos.Image = global::ProgramacionCapas.Properties.Resources.D1;
            this.pbxDadoDos.Location = new System.Drawing.Point(554, 32);
            this.pbxDadoDos.Name = "pbxDadoDos";
            this.pbxDadoDos.Size = new System.Drawing.Size(158, 158);
            this.pbxDadoDos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxDadoDos.TabIndex = 0;
            this.pbxDadoDos.TabStop = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(751, 481);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gbJugDos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbJugUno);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.gbJugUno.ResumeLayout(false);
            this.gbJugUno.PerformLayout();
            this.gbJugDos.ResumeLayout(false);
            this.gbJugDos.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDadoUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDadoDos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbJugUno;
        private System.Windows.Forms.Label lblIntUno;
        private System.Windows.Forms.Label lblPunUno;
        private System.Windows.Forms.GroupBox gbJugDos;
        private System.Windows.Forms.Label lblIntDos;
        private System.Windows.Forms.Label lblPunDos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnLanzar;
        private System.Windows.Forms.TextBox txtNum;
        private System.Windows.Forms.PictureBox pbxDadoUno;
        private System.Windows.Forms.PictureBox pbxDadoDos;
    }
}