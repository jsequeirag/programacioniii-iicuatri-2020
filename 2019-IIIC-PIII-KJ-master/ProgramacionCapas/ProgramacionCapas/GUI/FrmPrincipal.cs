﻿using ProgramacionCapas.BOL;
using ProgramacionCapas.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionCapas.GUI
{
    public partial class FrmPrincipal : Form
    {
        public LogicaBOL log;

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            log = new LogicaBOL();
            ValidarUsuarios();
        }

        private void ValidarUsuarios()
        {
            if (log.JugadorUno == null || log.JugadorDos == null)
            {
                FrmRegistro frm = new FrmRegistro { Logica = log };
                Hide();
                frm.ShowDialog();
                Iniciar();
                Show();
            }
        }

        private void Iniciar()
        {
            gbJugUno.Text = log.JugadorUno?.Nombre;
            gbJugDos.Text = log.JugadorDos?.Nombre;
            log.Rifar();
            Pintar();
        }

        private void Pintar()
        {
            lblPunUno.Text = log.JugadorUno?.PuntajeActual.Puntaje.ToString();
            lblPunDos.Text = log.JugadorDos?.PuntajeActual.Puntaje.ToString();
            lblIntUno.Text = log.JugadorUno?.PuntajeActual.Lanzamientos.ToString() + "/" + LogicaBOL.MAX_INT;
            lblIntDos.Text = log.JugadorDos?.PuntajeActual.Lanzamientos.ToString() + "/" + LogicaBOL.MAX_INT;

            gbJugUno.BackColor = log.JugadorUno == log.Actual ? SystemColors.ActiveCaption : SystemColors.Window;
            gbJugDos.BackColor = log.JugadorDos == log.Actual ? SystemColors.ActiveCaption : SystemColors.Window;
        }

        private void txtNum_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNum.Text.Trim().Length > 0)
                {
                    int num = int.Parse(txtNum.Text);
                    btnLanzar.Enabled = num >= 2 && num <= 12;
                }
                else
                {
                    btnLanzar.Enabled = false;
                }
            }
            catch (Exception)
            {
                btnLanzar.Enabled = false;
                txtNum.Text = "";
            }
        }

        private void btnLanzar_Click(object sender, EventArgs e)
        {
            btnLanzar.Enabled = false;
            Random r = new Random();
            int d1 = 0;
            int d2 = 0;
            for (int i = 0; i < 10; i++)
            {
                d1 = r.Next(1, 7);
                d2 = r.Next(1, 7);
                pbxDadoUno.Image = (Image)Resources.ResourceManager.GetObject("D" + d1);
                pbxDadoDos.Image = (Image)Resources.ResourceManager.GetObject("D" + d2);
                Refresh();
                Thread.Sleep(20 * i);
            }
            int num = int.Parse(txtNum.Text.Trim());
            log.Revisar(d1, d2, num);
            Pintar();
            Termino();
        }

        private void Termino()
        {
            if (log.JugadorUno.PuntajeActual.Lanzamientos == LogicaBOL.MAX_INT
                && log.JugadorDos.PuntajeActual.Lanzamientos == LogicaBOL.MAX_INT)
            {
                log.RegistrarPuntaje();
                log.JugadorUno = null;
                log.JugadorDos = null;
            }
            else if(log.DiferenciaAbismal()){
                log.RegistrarPuntaje();
                log.JugadorUno = null;
                log.JugadorDos = null;
            }

            
            ValidarUsuarios();
        }
    }
}
