﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using WebService.IMN;
using WebService.WSBCCR;
using static WebService.IMNEntities;

namespace WebService
{
    public partial class Form1 : Form
    {
        private double tcVenta;
        public Form1()
        {
            InitializeComponent();
            tcVenta = 500;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarTipoCambio();

            /*

            < Datos_de_INGC011_CAT_INDICADORECONOMIC >
                < INGC011_CAT_INDICADORECONOMIC >
                    < COD_INDICADORINTERNO > 318 </ COD_INDICADORINTERNO >
                    < DES_FECHA > 2019 - 11 - 21T00: 00:00 - 06:00 </ DES_FECHA >
                    < NUM_VALOR > 576.71000000 </ NUM_VALOR 
                </ INGC011_CAT_INDICADORECONOMIC >
            </ Datos_de_INGC011_CAT_INDICADORECONOMIC >
            
          */
        }

        private void CargarTipoCambio()
        {
            try
            {
                wsindicadoreseconomicosSoapClient ws = new wsindicadoreseconomicosSoapClient("wsindicadoreseconomicosSoap");
                string xml = ws.ObtenerIndicadoresEconomicosXML("318", dtpFecha.Value.ToString("dd/MM/yyyy"),
                    dtpFecha.Value.ToString("dd/MM/yyyy"), "Allan", "N", "amurilloa@utn.ac.cr", "UL0TAL122L");
                Console.WriteLine(xml);

                var indicador = xml.ParseXML<BCCREntities.Datos_de_INGC011_CAT_INDICADORECONOMIC>();
                Console.WriteLine(indicador.INGC011_CAT_INDICADORECONOMIC.DES_FECHA);
                Console.WriteLine(indicador.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR);
                Console.WriteLine(indicador.INGC011_CAT_INDICADORECONOMIC.COD_INDICADORINTERNO);

                XDocument xml1 = XDocument.Parse(xml);
                string valor = xml1.Descendants("NUM_VALOR").ToList().Last().Value;
                tcVenta = Double.Parse(valor);
            }
            catch (Exception)
            {
                MessageBox.Show("No es posible consultar el tipo de cambio, tipo de cambio de referencia " + tcVenta);
            }
        }

        private void txtUSD_TextChanged(object sender, EventArgs e)
        {
            if (txtUSD.Focused)
            {
                DolaresAColones();
            }
        }

        private void txtCRC_TextChanged(object sender, EventArgs e)
        {
            if (txtCRC.Focused)
            {
                ColonesADolares();
            }
        }

        private void ColonesADolares()
        {
            string txt = txtCRC.Text.Trim();
            if (!string.IsNullOrEmpty(txt))
            {
                double valor = double.Parse(txt);
                txtUSD.Text = (valor / tcVenta).ToString("N2");
            }
            else
            {
                txtUSD.Text = "";
            }
        }

        private void DolaresAColones()
        {
            string txt = txtUSD.Text.Trim();
            if (!string.IsNullOrEmpty(txt))
            {
                double valor = double.Parse(txt);
                txtCRC.Text = (valor * tcVenta).ToString("N2");
            }
            else
            {
                txtCRC.Text = "";
            }
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            CargarTipoCambio();
            txtCRC.Clear();
            txtUSD.Clear();
        }
    }
}
