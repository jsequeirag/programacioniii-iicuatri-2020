--drop table visor.imagenes
create table visor.imagenes(
	id serial primary key,
	imagen bytea not null, 
	titulo text not null, 
	tipo text not null,
	descripcion text
);

select * from visor.imagenes