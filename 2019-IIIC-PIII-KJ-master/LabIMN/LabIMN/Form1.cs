﻿using LabIMN.WSIMN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static LabIMN.WSIMNEntities;

namespace LabIMN
{
    public partial class Form1 : Form
    {
        private string RUTA_EFE = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/Luna-{0}-blanco.png";
        private string RUTA_EST = "https://www.imn.ac.cr/{0}";

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            WSIMNEntities.EFEMERIDES temp = ws.efemerides(new efemerides()).ParseXML<WSIMNEntities.EFEMERIDES>();
            string fecha = ws.fecha("").ToClean();
            fecha = fecha.Replace(fecha.ElementAt(0), Char.ToUpper(fecha.ElementAt(0)));
            lblFecha.Text = fecha;
            pbFaseLunar.ImageLocation = string.Format(RUTA_EFE, temp.FASELUNAR.Value.Replace(" ", "-").ToLower());
            toolTip1.ToolTipTitle = "Fase Lunar";
            toolTip1.SetToolTip(pbFaseLunar, temp.FASELUNAR.Value);

            toolTip2.SetToolTip(pictureBox1, String.Format("Sale: {0}\nSe pone: {1}", temp.EFEMERIDE_SOL.SALE, temp.EFEMERIDE_SOL.SEPONE));
            toolTip3.SetToolTip(pictureBox2, String.Format("Sale: {0}\nSe pone: {1}", temp.EFEMERIDE_LUNA.SALE, temp.EFEMERIDE_LUNA.SEPONE));

            WSIMNEntities.PRONOSTICO_REGIONAL data = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<WSIMNEntities.PRONOSTICO_REGIONAL>();
            comboBox1.DataSource = data.REGIONES;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox1.ReadOnly = false;
            PRONOSTICO_REGIONALREGION region = comboBox1.SelectedItem as PRONOSTICO_REGIONALREGION;
            comboBox2.DataSource = region.CIUDADES;
            richTextBox1.Clear();

            Pegar(richTextBox1, region.ESTADOMADRUGADA?.imgPath);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.AppendText(" Madrugada: ");
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.AppendText(region.COMENTARIOMADRUGADA ?? "Sin comentario");

            Pegar(richTextBox1, region.ESTADOMANANA?.imgPath);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.AppendText(" Mañana: ");
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.AppendText(region.COMENTARIOMANANA ?? "Sin comentario");

            Pegar(richTextBox1, region.ESTADOTARDE?.imgPath);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.AppendText(" Tarde: ");
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.AppendText(region.COMENTARIOTARDE ?? "Sin comentario");

            Pegar(richTextBox1, region.ESTADONOCHE?.imgPath);
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox1.AppendText(" Noche: ");
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.AppendText(region.COMENTARIONOCHE ?? "Sin comentario");
            richTextBox1.ReadOnly = true;

        }

        private void Pegar(RichTextBox rich, string imgPath)
        {
            rich.AppendText("\n\n");
            if (imgPath != null)
            {
                Clipboard.SetImage(Descargar(imgPath));
                rich.Paste();
            }
        }

        private Image Descargar(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(string.Format(RUTA_EST, url));
            MemoryStream ms = new MemoryStream(bytes);
            Image img = Image.FromStream(ms);

            return ResizeImage(img, new Size(32, 32));
        }

        public Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox2.ReadOnly = false; 
             PRONOSTICO_REGIONALREGIONCIUDAD ciudad = comboBox2.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD;
            richTextBox2.Clear();

            richTextBox2.AppendText("\nTemp Max. " + ciudad.TEMPMAX);
            richTextBox2.AppendText("\nTemp Min. " + ciudad.TEMPMIN);
            Pegar(richTextBox2, ciudad.ESTADOMADRUGADA?.imgPath);
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox2.AppendText(" Madrugada: ");
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox2.AppendText(ciudad.ESTADOMADRUGADA?.Value ?? "Sin comentario");

            Pegar(richTextBox2, ciudad.ESTADOMANANA?.imgPath);
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox2.AppendText(" Mañana: ");
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox2.AppendText(ciudad.ESTADOMANANA?.Value ?? "Sin comentario");

            Pegar(richTextBox2, ciudad.ESTADOTARDE?.imgPath);
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox2.AppendText(" Tarde: ");
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox2.AppendText(ciudad.ESTADOTARDE?.Value ?? "Sin comentario");

            Pegar(richTextBox2, ciudad.ESTADONOCHE?.imgPath);
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
            richTextBox2.AppendText(" Noche: ");
            richTextBox2.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox2.AppendText(ciudad.ESTADONOCHE?.Value ?? "Sin comentario");
            richTextBox2.ReadOnly = true;
        }
    }
}
