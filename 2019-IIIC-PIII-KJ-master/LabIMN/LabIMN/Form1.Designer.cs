﻿namespace LabIMN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pbFaseLunar = new System.Windows.Forms.PictureBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblFecha = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOSTICOREGIONALREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbFaseLunar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // pbFaseLunar
            // 
            this.pbFaseLunar.AccessibleDescription = "asdasd";
            this.pbFaseLunar.Location = new System.Drawing.Point(749, 126);
            this.pbFaseLunar.Name = "pbFaseLunar";
            this.pbFaseLunar.Size = new System.Drawing.Size(66, 68);
            this.pbFaseLunar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFaseLunar.TabIndex = 5;
            this.pbFaseLunar.TabStop = false;
            this.toolTip1.SetToolTip(this.pbFaseLunar, "Fase Lunar");
            // 
            // toolTip2
            // 
            this.toolTip2.IsBalloon = true;
            this.toolTip2.ToolTipTitle = "Efemeride Sol";
            // 
            // toolTip3
            // 
            this.toolTip3.IsBalloon = true;
            this.toolTip3.ToolTipTitle = "Efeméride Luna";
            // 
            // pictureBox2
            // 
            this.pictureBox2.AccessibleDescription = "asdasd";
            this.pictureBox2.ImageLocation = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/efemerides-lun" +
    "a-sale%20y%20pone-blanco.png?pfdrid_c=true";
            this.pictureBox2.Location = new System.Drawing.Point(748, 314);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(68, 77);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.AccessibleDescription = "asdasd";
            this.pictureBox1.ImageLocation = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/efemerides-sol" +
    "-sale%20y%20pone-blanco.png?pfdrid_c=true";
            this.pictureBox1.Location = new System.Drawing.Point(744, 220);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(77, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // lblFecha
            // 
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(475, 11);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(340, 28);
            this.lblFecha.TabIndex = 4;
            this.lblFecha.Text = "Fecha";
            this.lblFecha.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.pRONOSTICOREGIONALREGIONBindingSource;
            this.comboBox1.DisplayMember = "nombre";
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 73);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(344, 37);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.ValueMember = "idRegion";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // pRONOSTICOREGIONALBindingSource
            // 
            this.pRONOSTICOREGIONALBindingSource.DataSource = typeof(LabIMN.WSIMNEntities.PRONOSTICO_REGIONAL);
            // 
            // pRONOSTICOREGIONALREGIONBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONBindingSource.DataSource = typeof(LabIMN.WSIMNEntities.PRONOSTICO_REGIONALREGION);
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.pRONOSTICOREGIONALREGIONCIUDADBindingSource;
            this.comboBox2.DisplayMember = "nombre";
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(381, 73);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(344, 37);
            this.comboBox2.TabIndex = 9;
            this.comboBox2.ValueMember = "id";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // pRONOSTICOREGIONALREGIONCIUDADBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource.DataSource = typeof(LabIMN.WSIMNEntities.PRONOSTICO_REGIONALREGIONCIUDAD);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(612, 28);
            this.label1.TabIndex = 10;
            this.label1.Text = "Pronóstico Regional";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 126);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(344, 448);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(381, 126);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(344, 448);
            this.richTextBox2.TabIndex = 12;
            this.richTextBox2.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(831, 586);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbFaseLunar);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbFaseLunar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbFaseLunar;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONBindingSource;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALBindingSource;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONCIUDADBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}

