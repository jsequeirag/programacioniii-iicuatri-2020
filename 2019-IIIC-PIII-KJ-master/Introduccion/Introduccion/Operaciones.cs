﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduccion
{
    class Operaciones
    {

        private int[] arreglo;

        /// <summary>
        /// Suma dos números enteros
        /// </summary>
        /// <param name="n1">Numero 1</param>
        /// <param name="n2">Numero 2</param>
        /// <returns>int con la suma de numero 1 con numero dos </returns>
        internal double Calcular(int n1, int n2, int op)
        {
            switch (op)
            {
                case 1:
                    return n1 + n2;
                case 2:
                    return n1 - n2;
                case 3:
                    return n1 * n2;
                default:
                    return n2 == 0 ? 0 : (double)n1 / n2;
            }
        }

        /// <summary>
        /// Inicializa el arreglo y lo llena con números aleatorios
        /// </summary>
        /// <param name="dim">Nuevo tamaño del arreglo</param>
        internal void LlenarArreglo(int dim)
        {
            Random ran = new Random();
            arreglo = new int[dim];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = ran.Next(dim + 1);
            }
        }

        internal string ImprimirArreglo()
        {
            string txt = "";
            foreach (int item in arreglo)
            {
                txt += String.Format("{0}, ", item);
            }
            return txt.Substring(0, txt.Length - 2);
        }
    }
}
