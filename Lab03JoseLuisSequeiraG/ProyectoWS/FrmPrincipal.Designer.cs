﻿namespace ProyectoWS
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.sol = new System.Windows.Forms.Button();
            this.lblefe = new System.Windows.Forms.Label();
            this.luna = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFase = new System.Windows.Forms.Label();
            this.faseLunar = new System.Windows.Forms.Button();
            this.lblsaleSol = new System.Windows.Forms.Label();
            this.lblponeLuna = new System.Windows.Forms.Label();
            this.lblponeSol = new System.Windows.Forms.Label();
            this.lblsaleLuna = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltemMin = new System.Windows.Forms.Label();
            this.lbltemMax = new System.Windows.Forms.Label();
            this.cbCiudad = new System.Windows.Forms.ComboBox();
            this.pnlNocheR = new System.Windows.Forms.Panel();
            this.lblComentarioN = new System.Windows.Forms.TextBox();
            this.lblTituloN = new System.Windows.Forms.Label();
            this.pbxImagenN = new System.Windows.Forms.PictureBox();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.pnlTardeR = new System.Windows.Forms.Panel();
            this.pbxImagenT = new System.Windows.Forms.PictureBox();
            this.lblTituloT = new System.Windows.Forms.Label();
            this.lblComentarioT = new System.Windows.Forms.TextBox();
            this.pnlMañanaR = new System.Windows.Forms.Panel();
            this.pbxImagenM = new System.Windows.Forms.PictureBox();
            this.lblTituloM = new System.Windows.Forms.Label();
            this.lblComentarioM = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlNocheR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).BeginInit();
            this.pnlTardeR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).BeginInit();
            this.pnlMañanaR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.panel2);
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.Controls.Add(this.cbCiudad);
            this.groupBox4.Controls.Add(this.cbRegion);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(12, 11);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(953, 625);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(522, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 20);
            this.label6.TabIndex = 24;
            this.label6.Text = "Ciudad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 20);
            this.label5.TabIndex = 23;
            this.label5.Text = "Region";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 561);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 35);
            this.button1.TabIndex = 22;
            this.button1.Text = "Cerrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 481);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(341, 25);
            this.label9.TabIndex = 3;
            this.label9.Text = "NOTA:No carga estado de la manaña";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.sol);
            this.panel2.Controls.Add(this.lblefe);
            this.panel2.Controls.Add(this.luna);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblFase);
            this.panel2.Controls.Add(this.faseLunar);
            this.panel2.Controls.Add(this.lblsaleSol);
            this.panel2.Controls.Add(this.lblponeLuna);
            this.panel2.Controls.Add(this.lblponeSol);
            this.panel2.Controls.Add(this.lblsaleLuna);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(526, 198);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(414, 375);
            this.panel2.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 25);
            this.label4.TabIndex = 20;
            this.label4.Text = "Efemérides(San josé)";
            // 
            // sol
            // 
            this.sol.Enabled = false;
            this.sol.Location = new System.Drawing.Point(248, 201);
            this.sol.Name = "sol";
            this.sol.Size = new System.Drawing.Size(96, 76);
            this.sol.TabIndex = 18;
            this.sol.UseVisualStyleBackColor = true;
            // 
            // lblefe
            // 
            this.lblefe.AutoSize = true;
            this.lblefe.Location = new System.Drawing.Point(243, 294);
            this.lblefe.Name = "lblefe";
            this.lblefe.Size = new System.Drawing.Size(58, 25);
            this.lblefe.TabIndex = 7;
            this.lblefe.Text = "Sale:";
            // 
            // luna
            // 
            this.luna.Enabled = false;
            this.luna.Location = new System.Drawing.Point(42, 201);
            this.luna.Name = "luna";
            this.luna.Size = new System.Drawing.Size(96, 76);
            this.luna.TabIndex = 19;
            this.luna.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "Se pone:";
            // 
            // lblFase
            // 
            this.lblFase.AutoSize = true;
            this.lblFase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFase.Location = new System.Drawing.Point(42, 135);
            this.lblFase.Name = "lblFase";
            this.lblFase.Size = new System.Drawing.Size(2, 27);
            this.lblFase.TabIndex = 9;
            // 
            // faseLunar
            // 
            this.faseLunar.Enabled = false;
            this.faseLunar.Location = new System.Drawing.Point(42, 48);
            this.faseLunar.Name = "faseLunar";
            this.faseLunar.Size = new System.Drawing.Size(96, 76);
            this.faseLunar.TabIndex = 17;
            this.faseLunar.UseVisualStyleBackColor = true;
            // 
            // lblsaleSol
            // 
            this.lblsaleSol.AutoSize = true;
            this.lblsaleSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblsaleSol.Location = new System.Drawing.Point(101, 294);
            this.lblsaleSol.Name = "lblsaleSol";
            this.lblsaleSol.Size = new System.Drawing.Size(66, 27);
            this.lblsaleSol.TabIndex = 11;
            this.lblsaleSol.Text = "label5";
            // 
            // lblponeLuna
            // 
            this.lblponeLuna.AutoSize = true;
            this.lblponeLuna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblponeLuna.Location = new System.Drawing.Point(307, 333);
            this.lblponeLuna.Name = "lblponeLuna";
            this.lblponeLuna.Size = new System.Drawing.Size(66, 27);
            this.lblponeLuna.TabIndex = 16;
            this.lblponeLuna.Text = "label6";
            // 
            // lblponeSol
            // 
            this.lblponeSol.AutoSize = true;
            this.lblponeSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblponeSol.Location = new System.Drawing.Point(101, 333);
            this.lblponeSol.Name = "lblponeSol";
            this.lblponeSol.Size = new System.Drawing.Size(66, 27);
            this.lblponeSol.TabIndex = 12;
            this.lblponeSol.Text = "label6";
            // 
            // lblsaleLuna
            // 
            this.lblsaleLuna.AutoSize = true;
            this.lblsaleLuna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblsaleLuna.Location = new System.Drawing.Point(307, 294);
            this.lblsaleLuna.Name = "lblsaleLuna";
            this.lblsaleLuna.Size = new System.Drawing.Size(66, 27);
            this.lblsaleLuna.TabIndex = 15;
            this.lblsaleLuna.Text = "label5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 294);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 25);
            this.label8.TabIndex = 13;
            this.label8.Text = "Sale:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 25);
            this.label7.TabIndex = 14;
            this.label7.Text = "Se pone:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lbltemMin);
            this.panel1.Controls.Add(this.lbltemMax);
            this.panel1.Location = new System.Drawing.Point(526, 79);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(414, 113);
            this.panel1.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Temperatura max(C°):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Temperatura min(C°):";
            // 
            // lbltemMin
            // 
            this.lbltemMin.AutoSize = true;
            this.lbltemMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbltemMin.Location = new System.Drawing.Point(285, 23);
            this.lbltemMin.Name = "lbltemMin";
            this.lbltemMin.Size = new System.Drawing.Size(2, 27);
            this.lbltemMin.TabIndex = 5;
            // 
            // lbltemMax
            // 
            this.lbltemMax.AutoSize = true;
            this.lbltemMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbltemMax.Location = new System.Drawing.Point(285, 70);
            this.lbltemMax.Name = "lbltemMax";
            this.lbltemMax.Size = new System.Drawing.Size(2, 27);
            this.lbltemMax.TabIndex = 6;
            // 
            // cbCiudad
            // 
            this.cbCiudad.DisplayMember = "nombre";
            this.cbCiudad.FormattingEnabled = true;
            this.cbCiudad.Location = new System.Drawing.Point(526, 37);
            this.cbCiudad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCiudad.Name = "cbCiudad";
            this.cbCiudad.Size = new System.Drawing.Size(414, 33);
            this.cbCiudad.TabIndex = 2;
            this.cbCiudad.ValueMember = "idRegion";
            this.cbCiudad.SelectedIndexChanged += new System.EventHandler(this.cbCiudad_SelectedIndexChanged);
            // 
            // pnlNocheR
            // 
            this.pnlNocheR.Controls.Add(this.lblComentarioN);
            this.pnlNocheR.Controls.Add(this.lblTituloN);
            this.pnlNocheR.Controls.Add(this.pbxImagenN);
            this.pnlNocheR.Location = new System.Drawing.Point(18, 274);
            this.pnlNocheR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlNocheR.Name = "pnlNocheR";
            this.pnlNocheR.Size = new System.Drawing.Size(348, 95);
            this.pnlNocheR.TabIndex = 2;
            // 
            // lblComentarioN
            // 
            this.lblComentarioN.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioN.Enabled = false;
            this.lblComentarioN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioN.Location = new System.Drawing.Point(104, 39);
            this.lblComentarioN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioN.Multiline = true;
            this.lblComentarioN.Name = "lblComentarioN";
            this.lblComentarioN.ReadOnly = true;
            this.lblComentarioN.Size = new System.Drawing.Size(232, 47);
            this.lblComentarioN.TabIndex = 2;
            this.lblComentarioN.Text = "Comentarios";
            // 
            // lblTituloN
            // 
            this.lblTituloN.AutoSize = true;
            this.lblTituloN.Location = new System.Drawing.Point(99, 10);
            this.lblTituloN.Name = "lblTituloN";
            this.lblTituloN.Size = new System.Drawing.Size(78, 25);
            this.lblTituloN.TabIndex = 1;
            this.lblTituloN.Text = "Estado ";
            // 
            // pbxImagenN
            // 
            this.pbxImagenN.Location = new System.Drawing.Point(3, 2);
            this.pbxImagenN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenN.Name = "pbxImagenN";
            this.pbxImagenN.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenN.TabIndex = 0;
            this.pbxImagenN.TabStop = false;
            // 
            // cbRegion
            // 
            this.cbRegion.DisplayMember = "nombre";
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(21, 37);
            this.cbRegion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(457, 33);
            this.cbRegion.TabIndex = 0;
            this.cbRegion.ValueMember = "idRegion";
            this.cbRegion.SelectedIndexChanged += new System.EventHandler(this.boxRegiones_SelectedIndexChanged);
            // 
            // pnlTardeR
            // 
            this.pnlTardeR.Controls.Add(this.lblComentarioT);
            this.pnlTardeR.Controls.Add(this.lblTituloT);
            this.pnlTardeR.Controls.Add(this.pbxImagenT);
            this.pnlTardeR.Location = new System.Drawing.Point(18, 145);
            this.pnlTardeR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTardeR.Name = "pnlTardeR";
            this.pnlTardeR.Size = new System.Drawing.Size(348, 95);
            this.pnlTardeR.TabIndex = 1;
            // 
            // pbxImagenT
            // 
            this.pbxImagenT.Location = new System.Drawing.Point(3, 5);
            this.pbxImagenT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenT.Name = "pbxImagenT";
            this.pbxImagenT.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenT.TabIndex = 0;
            this.pbxImagenT.TabStop = false;
            // 
            // lblTituloT
            // 
            this.lblTituloT.AutoSize = true;
            this.lblTituloT.Location = new System.Drawing.Point(99, 10);
            this.lblTituloT.Name = "lblTituloT";
            this.lblTituloT.Size = new System.Drawing.Size(78, 25);
            this.lblTituloT.TabIndex = 1;
            this.lblTituloT.Text = "Estado ";
            // 
            // lblComentarioT
            // 
            this.lblComentarioT.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioT.Enabled = false;
            this.lblComentarioT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioT.Location = new System.Drawing.Point(101, 42);
            this.lblComentarioT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioT.Multiline = true;
            this.lblComentarioT.Name = "lblComentarioT";
            this.lblComentarioT.ReadOnly = true;
            this.lblComentarioT.Size = new System.Drawing.Size(237, 47);
            this.lblComentarioT.TabIndex = 2;
            this.lblComentarioT.Text = "Comentarios";
            // 
            // pnlMañanaR
            // 
            this.pnlMañanaR.Controls.Add(this.lblComentarioM);
            this.pnlMañanaR.Controls.Add(this.lblTituloM);
            this.pnlMañanaR.Controls.Add(this.pbxImagenM);
            this.pnlMañanaR.Location = new System.Drawing.Point(18, 13);
            this.pnlMañanaR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlMañanaR.Name = "pnlMañanaR";
            this.pnlMañanaR.Size = new System.Drawing.Size(348, 95);
            this.pnlMañanaR.TabIndex = 0;
            // 
            // pbxImagenM
            // 
            this.pbxImagenM.Location = new System.Drawing.Point(3, 4);
            this.pbxImagenM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenM.Name = "pbxImagenM";
            this.pbxImagenM.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenM.TabIndex = 0;
            this.pbxImagenM.TabStop = false;
            // 
            // lblTituloM
            // 
            this.lblTituloM.AutoSize = true;
            this.lblTituloM.Location = new System.Drawing.Point(95, 14);
            this.lblTituloM.Name = "lblTituloM";
            this.lblTituloM.Size = new System.Drawing.Size(149, 25);
            this.lblTituloM.TabIndex = 1;
            this.lblTituloM.Text = "Estado manaña";
            // 
            // lblComentarioM
            // 
            this.lblComentarioM.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioM.Enabled = false;
            this.lblComentarioM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioM.Location = new System.Drawing.Point(101, 39);
            this.lblComentarioM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioM.Multiline = true;
            this.lblComentarioM.Name = "lblComentarioM";
            this.lblComentarioM.ReadOnly = true;
            this.lblComentarioM.Size = new System.Drawing.Size(237, 47);
            this.lblComentarioM.TabIndex = 2;
            this.lblComentarioM.Text = "Comentarios";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.pnlMañanaR);
            this.panel3.Controls.Add(this.pnlTardeR);
            this.panel3.Controls.Add(this.pnlNocheR);
            this.panel3.Location = new System.Drawing.Point(21, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(457, 371);
            this.panel3.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 242);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 25);
            this.label10.TabIndex = 3;
            this.label10.Text = "Estado Noche";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 25);
            this.label11.TabIndex = 4;
            this.label11.Text = "Estado Tarde";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 646);
            this.Controls.Add(this.groupBox4);
            this.Name = "FrmPrincipal";
            this.Text = "Pronóstico del Tiempo por Región";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlNocheR.ResumeLayout(false);
            this.pnlNocheR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).EndInit();
            this.pnlTardeR.ResumeLayout(false);
            this.pnlTardeR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).EndInit();
            this.pnlMañanaR.ResumeLayout(false);
            this.pnlMañanaR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbCiudad;
        private System.Windows.Forms.Panel pnlNocheR;
        private System.Windows.Forms.TextBox lblComentarioN;
        private System.Windows.Forms.Label lblTituloN;
        private System.Windows.Forms.PictureBox pbxImagenN;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.Label lbltemMax;
        private System.Windows.Forms.Label lbltemMin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblefe;
        private System.Windows.Forms.Label lblFase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblponeLuna;
        private System.Windows.Forms.Label lblsaleLuna;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblponeSol;
        private System.Windows.Forms.Label lblsaleSol;
        private System.Windows.Forms.Button luna;
        private System.Windows.Forms.Button sol;
        private System.Windows.Forms.Button faseLunar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pnlMañanaR;
        private System.Windows.Forms.TextBox lblComentarioM;
        private System.Windows.Forms.Label lblTituloM;
        private System.Windows.Forms.PictureBox pbxImagenM;
        private System.Windows.Forms.Panel pnlTardeR;
        private System.Windows.Forms.TextBox lblComentarioT;
        private System.Windows.Forms.Label lblTituloT;
        private System.Windows.Forms.PictureBox pbxImagenT;
    }
}

