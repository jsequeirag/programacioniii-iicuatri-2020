﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;
namespace CapaNegocio
{
    public class CNAvion
    {

        public void seleccionarAvion(DataGridView dgv) => new CDAvion().seleccionarAvion(dgv);

        public void insertarAvion(string modelo, string annio, int id_aerolinea, int capacidad, string estado) => new CDAvion().insertarAvion(modelo, annio, id_aerolinea, capacidad, estado);

        public void seleccionarAvioncantidad(DataGridView dgv) => new CDAvion().seleccionarAvioncantidad(dgv);

    }
}
