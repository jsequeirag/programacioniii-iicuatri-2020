﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos;

namespace CapaNegocio
{
    public class CNAerolinea
    {

        public void seleccionarAerolineas(DataGridView dgv) => new CDAerolinea().seleccionarAerolineas(dgv);


        public void insertarAerolinea(string nombre, string annio, string tipo) => new CDAerolinea().insertarAerolinea(nombre,annio,tipo);

        public void buscarAerolinea(DataGridView dgv,string palabra) => new CDAerolinea().buscarAerolinea(dgv,palabra);
    }   
}
