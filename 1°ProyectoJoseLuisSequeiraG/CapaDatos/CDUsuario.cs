﻿using Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
   public class CDUsuario
    {

        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();





        public string GetSHA256(string str)
        {
            SHA256 sha256 = SHA256Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha256.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }


        public Usuario seleccionarUsuario(int usuarioSesion, string contrasenna)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT id_usuario,cedula,nombre,edad, contrasenna, tipo FROM vl.usuario WHERE cedula ='{usuarioSesion}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            string hashedPassword = GetSHA256(contrasenna);
           
            Usuario usuario = new Usuario();


           

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                 


                    if (hashedPassword == dr.GetString(4))
                    {

                        usuario.Id = dr.GetInt32(0);
                        usuario.Cedula = dr.GetInt32(1);
                        usuario.Nombre = dr.GetString(2);
                        usuario.Edad = dr.GetInt32(3);
                        usuario.Contrasenna = dr.GetString(4);
                        usuario.Tipo= dr.GetString(5);

                    }
                    else
                    {
                        MessageBox.Show("Contraseña no coincide con el usuario");
                        usuario.Nombre = "";
                    }

                }

            }
            else {

                MessageBox.Show("Usuario NO encontrado");
                usuario.Nombre = "";
            }
            return usuario;
        }



        public void registrarUsuario(string nombre, int cedula, int edad, string contrasenna)
        {
            conexionRetorno = conexion.ConexionBD();

            string coontrassenaEncryp = GetSHA256(contrasenna);


            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.usuario ( cedula,nombre,edad,contrasenna,tipo)VALUES('{cedula}','{nombre}','{edad}','{coontrassenaEncryp}','Pasajero');", conexionRetorno);
            cmd.ExecuteNonQuery();


        }

    }
}
