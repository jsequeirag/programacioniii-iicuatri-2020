﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaDatos
{
   public class CDHistorial
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();


        public void insertarHistorial(int cedula, string pais_salida,string  pais_llegada, string pais_escala1, string pais_escala2,string fecha_compra, int duracion_viaje,double  costo,int idAvion)
        {
 


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.historial(cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo,id_avion)VALUES({ cedula}, '{ pais_salida}', '{ pais_llegada}', '{pais_escala1}', '{ pais_escala2}',' {fecha_compra}', { duracion_viaje}, {costo},{idAvion})", conexionRetorno);

            cmd.ExecuteNonQuery();


        }
        public void seleccionarHistorial(DataGridView dgv,int cedula)
        {


            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, duracion_viaje, costo,id_avion from vl.historial where cedula={cedula}", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    DataRow datarow1 = datatable.NewRow();

                    datarow1["Cedula"] = dr.GetInt32(0).ToString();
                    datarow1["Pais_salida"] = dr.GetString(1);
                    datarow1["Pais_llegada"] = dr.GetString(2);
                    datarow1["Pais_escala1"] = dr.GetString(3);
                    datarow1["Pais_escala2"] = dr.GetString(4);
                    datarow1["Fecha/hora compra"] = dr.GetString(5);
                    datarow1["Horas"] = dr.GetString(6).ToString();
                    datarow1["Costo"] = dr.GetDouble(7).ToString();
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;

                }

            }
        }

    }
}
