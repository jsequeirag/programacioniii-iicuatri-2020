﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;
namespace CapaDatos
{
   public class CDTripulacion
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();






        public void cargarTripulantesAerolinea(DataGridView dgvTripulacion, string nombreAerolinea) {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT t.nombre,t.cedula,t.fecha_nacimiento,t.rol,t.estado,a.nombre as Aerolinea FROM vl.aerolinea as a join vl.tripulacion as t on a.id_aerolinea = t.id_aerolinea where  a.nombre = '{nombreAerolinea}'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgvTripulacion.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Nombre"] = dr.GetString(0);
                    datarow1["Cedula"] = dr.GetInt32(1).ToString();
                    datarow1["Fecha de nacimiento"] = dr.GetDate(2).ToString();
                    datarow1["Rol"] = dr.GetString(3);
                    datarow1["Estado"] = dr.GetString(4);
                    datarow1["Aerolinea"] = dr.GetString(5);
                    datatable.Rows.Add(datarow1);
                    dgvTripulacion.DataSource = datatable;

                }

            }

        }


        public void seleccionarTripulacion(DataGridView dgv)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre,cedula,fecha_nacimiento,id_aerolinea,rol,estado  FROM vl.tripulacion ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Nombre"] = dr.GetString(0);
                    datarow1["Cedula"] = dr.GetInt32(1);
                    datarow1["Fecha de nacimiento"] = dr.GetDate(2).ToString();
                    datarow1["Id_aerolinea"] = dr.GetInt32(3).ToString();
                    datarow1["Rol"] = dr.GetString(4);
                    datarow1["Estado"] = dr.GetString(5);
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;

                }

            }
        }

        public void soloPiloto(List<string> lista)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id_tripulacion  FROM vl.tripulacion where rol='Piloto' and estado='Disponible' ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            


            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    lista.Add(dr.GetInt32(0).ToString()); ;
                    Console.WriteLine(dr.GetInt32(0).ToString());

                }

            }
            dr.Close();
        }

        public void soloServicio(List<string> lista)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id_tripulacion  FROM vl.tripulacion where rol='Servicio al cliente' and estado='Disponible'", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

          


            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    lista.Add(dr.GetInt32(0).ToString()); ;
                    Console.WriteLine(dr.GetInt32(0).ToString());

                }
                dr.Close();
            }
           
        }



        public void buscarTripulacion(DataGridView dgv, string palabra)
        {
            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"SELECT nombre,cedula,fecha_nacimiento,id_aerolinea,rol,estado  FROM vl.tripulacion where LOWER(nombre) LIKE LOWER('{palabra}%') ", conexionRetorno);

            NpgsqlDataReader dr = cmd.ExecuteReader();

            DataTable datatable = (DataTable)dgv.DataSource;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DataRow datarow1 = datatable.NewRow();
                    datarow1["Nombre"] = dr.GetString(0);
                    datarow1["Cedula"] = dr.GetInt32(1);
                    datarow1["Fecha de nacimiento"] = dr.GetDate(2).ToString();
                    datarow1["Rol"] = dr.GetString(3);
                    datarow1["Estado"] = dr.GetString(4);
                    datatable.Rows.Add(datarow1);
                    dgv.DataSource = datatable;

                }

            }


            
        }


        public void insertarTripulacion(string nombre, int cedula, string fecha_nacimiento,int id_aerolinea, string rol, string estado)
        {

            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"INSERT INTO vl.tripulacion  (nombre,cedula,fecha_nacimiento,id_aerolinea,rol,estado)VALUES('{nombre}','{cedula}','{fecha_nacimiento}','{id_aerolinea}','{rol}','{estado}');", conexionRetorno);

            cmd.ExecuteNonQuery();


        }


        public void modificarTripulacion(int id_Aerolinea)
        {

            conexionRetorno = conexion.ConexionBD();

            NpgsqlCommand cmd = new NpgsqlCommand($"UPDATE vl.tripulacion  SET estado='En servicio'  WHERE id_tripulacion={id_Aerolinea};", conexionRetorno);

            cmd.ExecuteNonQuery();


        }
    }

}


