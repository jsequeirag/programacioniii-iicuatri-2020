﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmPasajeroViajeVuelta : Form
    {

        string aeroSda;
        string aeroLda;

        DataTable dataTableescala1;
        DataTable dataTableescala2;
        DataTable dataTableescala3;

        public FrmPasajeroViajeVuelta(string aeroSda,string aeroLda)
        {
            InitializeComponent();

            this.aeroSda = aeroSda;
            this.aeroLda = aeroLda;
        }




        public void definirdgvEscala1()
        {


            dataTableescala1 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala1.Columns.Add(precio);
            dataTableescala1.Columns.Add("Fecha_Salida");
            dataTableescala1.Columns.Add("Aeropuerto salida");
            dataTableescala1.Columns.Add("Fecha llegada");
            dataTableescala1.Columns.Add("Aeropuerto llegada");
            dataTableescala1.Columns.Add("Duracion(hrs)");

            dgvEscala1.DataSource = dataTableescala1;


        }

        public void definirdgvEscala2()
        {


            dataTableescala2 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala2.Columns.Add(precio);
            dataTableescala2.Columns.Add("Fecha_Salida");
            dataTableescala2.Columns.Add("Aeropuerto salida");
            dataTableescala2.Columns.Add("Fecha llegada");
            dataTableescala2.Columns.Add("Aeropuerto llegada");
            dataTableescala2.Columns.Add("Duracion(hrs)");

            dgvEscala2.DataSource = dataTableescala2;


        }
        public void definirdgvEscala3()
        {

            dataTableescala3 = new DataTable();


            DataColumn precio = new DataColumn("Precio");
            precio.DataType = Type.GetType("System.Int32");

            dataTableescala3.Columns.Add(precio);
            dataTableescala3.Columns.Add("Fecha_Salida");
            dataTableescala3.Columns.Add("Aeropuerto salida");
            dataTableescala3.Columns.Add("Fecha llegada");
            dataTableescala3.Columns.Add("Aeropuerto llegada");
            dataTableescala3.Columns.Add("Duracion(hrs)");

            dgvEscala3.DataSource = dataTableescala3;


        }

       

        private void btnConfirmarEscala_Click(object sender, EventArgs e)
        {


        }
    }
}
