﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmReportMenu : Form
    {
        FrmMantenimiento frmMant;
        public FrmReportMenu(FrmMantenimiento frmMant)
        {

            InitializeComponent();
            this.frmMant = frmMant;
            CenterToScreen();
        }
        public FrmReportMenu(){
            InitializeComponent();
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmReportFecha frm = new FrmReportFecha(this);
            frm.Visible = true;
            this.Hide();

        }

 

        private void btnAerolinea_Click(object sender, EventArgs e)
        {

            FrmReportAerolinea frm = new FrmReportAerolinea(this);
            frm.Visible = true;
            this.Hide();

        }

        private void btnAvion_Click(object sender, EventArgs e)
        {
            FrmReportAvion frm = new FrmReportAvion(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnReportmimtri_Click(object sender, EventArgs e)
        {
            FrmReportTripulacion frm = new FrmReportTripulacion(this);
            frm.Visible = true;
            this.Hide();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmMantenimiento frm = new FrmMantenimiento();
            frm.Visible = true;
            this.Hide();
        }
    }
}
