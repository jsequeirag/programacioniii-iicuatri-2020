﻿namespace CapaPresentacion
{
    partial class FrmMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAerolineas = new System.Windows.Forms.Button();
            this.btnTripulaciones = new System.Windows.Forms.Button();
            this.btnAviones = new System.Windows.Forms.Button();
            this.btnAeropuertos = new System.Windows.Forms.Button();
            this.btnVuelos = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReporte = new System.Windows.Forms.Button();
            this.btnAtras = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAerolineas
            // 
            this.btnAerolineas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAerolineas.Location = new System.Drawing.Point(45, 46);
            this.btnAerolineas.Name = "btnAerolineas";
            this.btnAerolineas.Size = new System.Drawing.Size(290, 41);
            this.btnAerolineas.TabIndex = 0;
            this.btnAerolineas.Text = "Mantenimiento Aerolíneas";
            this.btnAerolineas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAerolineas.UseVisualStyleBackColor = true;
            this.btnAerolineas.Click += new System.EventHandler(this.btnAerolineas_Click);
            // 
            // btnTripulaciones
            // 
            this.btnTripulaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTripulaciones.Location = new System.Drawing.Point(45, 90);
            this.btnTripulaciones.Name = "btnTripulaciones";
            this.btnTripulaciones.Size = new System.Drawing.Size(290, 40);
            this.btnTripulaciones.TabIndex = 1;
            this.btnTripulaciones.Text = "Mantenimiento de Tripulaciones";
            this.btnTripulaciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTripulaciones.UseVisualStyleBackColor = true;
            this.btnTripulaciones.Click += new System.EventHandler(this.btnTripulaciones_Click);
            // 
            // btnAviones
            // 
            this.btnAviones.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAviones.Location = new System.Drawing.Point(46, 133);
            this.btnAviones.Name = "btnAviones";
            this.btnAviones.Size = new System.Drawing.Size(289, 42);
            this.btnAviones.TabIndex = 2;
            this.btnAviones.Text = "Mantenimiento de Aviones";
            this.btnAviones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAviones.UseVisualStyleBackColor = true;
            this.btnAviones.Click += new System.EventHandler(this.btnAviones_Click);
            // 
            // btnAeropuertos
            // 
            this.btnAeropuertos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAeropuertos.Location = new System.Drawing.Point(46, 178);
            this.btnAeropuertos.Name = "btnAeropuertos";
            this.btnAeropuertos.Size = new System.Drawing.Size(289, 44);
            this.btnAeropuertos.TabIndex = 3;
            this.btnAeropuertos.Text = "Mantenimiento de Aeropuertos";
            this.btnAeropuertos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAeropuertos.UseVisualStyleBackColor = true;
            this.btnAeropuertos.Click += new System.EventHandler(this.btnAeropuertos_Click);
            // 
            // btnVuelos
            // 
            this.btnVuelos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVuelos.Location = new System.Drawing.Point(46, 228);
            this.btnVuelos.Name = "btnVuelos";
            this.btnVuelos.Size = new System.Drawing.Size(289, 39);
            this.btnVuelos.TabIndex = 4;
            this.btnVuelos.Text = "Mantenimiento de Vuelos";
            this.btnVuelos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVuelos.UseVisualStyleBackColor = true;
            this.btnVuelos.Click += new System.EventHandler(this.btnVuelos_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(118, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mantenimiento";
            // 
            // btnReporte
            // 
            this.btnReporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte.Location = new System.Drawing.Point(45, 273);
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(289, 39);
            this.btnReporte.TabIndex = 6;
            this.btnReporte.Text = "Reportes";
            this.btnReporte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReporte.UseVisualStyleBackColor = true;
            this.btnReporte.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(13, 411);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 7;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // FrmMantenimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 446);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.btnReporte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVuelos);
            this.Controls.Add(this.btnAeropuertos);
            this.Controls.Add(this.btnAviones);
            this.Controls.Add(this.btnTripulaciones);
            this.Controls.Add(this.btnAerolineas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMantenimiento";
            this.Text = "FrmMantenimiento";
            this.Load += new System.EventHandler(this.FrmMantenimiento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAerolineas;
        private System.Windows.Forms.Button btnTripulaciones;
        private System.Windows.Forms.Button btnAviones;
        private System.Windows.Forms.Button btnAeropuertos;
        private System.Windows.Forms.Button btnVuelos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.Button btnAtras;
    }
}