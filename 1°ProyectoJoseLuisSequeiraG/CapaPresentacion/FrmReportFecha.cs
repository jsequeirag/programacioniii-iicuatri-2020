﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
namespace CapaPresentacion
{
    public partial class FrmReportFecha : Form
    {

        FrmReportMenu frmMant;
        public FrmReportFecha(FrmReportMenu frmMant)
        {
            InitializeComponent();
            CenterToScreen();
            this.frmMant = frmMant;
            definirDvg();
            cargarVuelo();
        }


        public void definirDvg() {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Precio");
            dataTable.Columns.Add("Fecha Salida");
            dataTable.Columns.Add("Aeropuerto salida");
            dataTable.Columns.Add("Fecha llegada");
            dataTable.Columns.Add("Aeropuerto llegada");
            dataTable.Columns.Add("Id_avion");
            dataTable.Columns.Add("Id_piloto1");
            dataTable.Columns.Add("Id_piloto2");
            dataTable.Columns.Add("Id_servicio1");
            dataTable.Columns.Add("Id_servicio2");
            dataTable.Columns.Add("Id_servicio3");

            dgvVuelo.DataSource = dataTable;


        }


        public void cargarVuelo() {

            string fechaInicial=dtpFechainicial.Value.ToString();
            string fechaFinal=dtpFechafinal.Value.ToString();
            

            new CNVuelo().reportePorfecha(dgvVuelo,fechaInicial,fechaFinal);
        
        }

        private void dtpFechainicial_ValueChanged(object sender, EventArgs e)
        {
            dgvVuelo.Columns.Clear();
            definirDvg();
            cargarVuelo();
        }

        private void dtpFechafinal_ValueChanged(object sender, EventArgs e)
        {
            dgvVuelo.Columns.Clear();
            definirDvg();
            cargarVuelo();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.frmMant.Visible = true;
            this.Hide();
        }
    }
}
