﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{

    public partial class FrmMantVueloReg : Form
    {
        FrmMantenimiento frmMant;

        List<string> listaServicio;

        List<string> listaPilotos;


        DataTable dataTablelda;


        int idPiloto;

        int idServicio;

        int id_avion;

        int id_aerolinea;

        string nombreAerosda;

        string nombreAerolda;



        public FrmMantVueloReg(FrmMantenimiento frmMant)
        {
            InitializeComponent();
            this.frmMant = frmMant;
            CenterToParent();

            cargarServicio();

            cargarAerolineas();

            definirDataAeropuertolda();
            definirDataAeropuertosda();

            cargarDataAeropuerto();

            definirAvion();

            cargarAvion();

            cargarPiloto();

            cargarServicio();

            MessageBox.Show((dtpFechasalida.Value.Date + dtpHorasalida.Value.TimeOfDay).ToString("dd/MM/yy hh:mm"));

            correrlistar();

        }

        public void definirDataAeropuertolda()
        {

            this.dataTablelda = new DataTable();

            dataTablelda.Columns.Add("Id_aeropuerto");
            dataTablelda.Columns.Add("IATA");
            dataTablelda.Columns.Add("Nombre");
            dataTablelda.Columns.Add("Pais");


            dgvaeroLlegada.DataSource = dataTablelda;
        }

        public void definirDataAeropuertosda()
        {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id_aeropuerto");
            dataTable.Columns.Add("IATA");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Pais");

            dgvaeroSalida.DataSource = dataTable;

        }




        public void cargarDataAeropuerto()
        {
            new CNAeropuerto().selecccionarAeropuerto(dgvaeroSalida, dgvaeroLlegada);

        }





        public void definirDataAerolinea()
        {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Año");
            dataTable.Columns.Add("Tipo");

            dgvAerolinea.DataSource = dataTable;
        }


        public void cargarAerolineas()
        {
            definirDataAerolinea();
            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);



        }





        public void definirDataGridview()
        {

            DataTable dataTable = new DataTable();

            //  DataColumn edad = new DataColumn("Edad");
            //  edad.DataType = Type.GetType("System.Int32");

            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Año");
            dataTable.Columns.Add("Tipo");

            dgvAerolinea.DataSource = dataTable;

            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);

        }


        public void definirAvion()
        {
            DataTable dataAvion = new DataTable();
            dataAvion = new DataTable();
            dataAvion.Columns.Add("Id_avion");
            dataAvion.Columns.Add("Modelo");
            dataAvion.Columns.Add("Annio_construccion");
            dataAvion.Columns.Add("Id_aerolinea");
            dataAvion.Columns.Add("Capacidad");
            dataAvion.Columns.Add("Estado");
            dgvAvion.DataSource = dataAvion;


        }

        public Boolean validacionInsertar() {
            bool booleana = false;

            if (listaPilotos.Count != 0 && listaServicio.Count != 0)
            {
                booleana = true;
            }

            return booleana;
        }


        public void cargarAvion()
        {

            new CNAvion().seleccionarAvion(dgvAvion);

        }



        public void correrlistar() {

            Console.WriteLine("Servicio");

            for (int x = 0; x < listaServicio.Count; x++) {


                Console.WriteLine(listaServicio[x]);



            }
            Console.WriteLine("Pilotos");
            for (int x = 0; x < listaPilotos.Count; x++)
            {
                Console.WriteLine(listaPilotos[x]);

            }


        }


        public void generarIdservicio() {


            if (listaServicio.Count != 0)
            {
                int randomNumber = new Random().Next(0, listaServicio.Count - 1);


                new CNTripulacion().modificarTripulacion(int.Parse(listaServicio[randomNumber]));


                Console.WriteLine("Numero radom" + randomNumber);

                Console.WriteLine("Id eliminado" + listaServicio[randomNumber]);

                idServicio = int.Parse(listaServicio[randomNumber]);




            } else

            {

                MessageBox.Show("No hay tripulación disponible");

            }



        }

        public void generarIdpiloto() {



            if (listaPilotos.Count != 0)
            {

                int randomNumber = new Random().Next(0, listaPilotos.Count - 1);

                Console.WriteLine(randomNumber);

                new CNTripulacion().modificarTripulacion(int.Parse(listaPilotos[randomNumber]));


                Console.WriteLine("Numero radom" + randomNumber);

                Console.WriteLine("Id eliminado" + listaPilotos[randomNumber]);

                idPiloto = int.Parse(listaPilotos[randomNumber]);
            }
            else {

                MessageBox.Show("No hay tripulación disponible");

            }

        }






        public void cargarPiloto() {

            listaPilotos = new List<string>();

            new CNTripulacion().seleccionarPiloto(listaPilotos);
        }
        public void cargarServicio()
        {

            listaServicio = new List<string>();

            new CNTripulacion().seleccionarServicio(listaServicio);

        }



        private void FrmVuelo_Load(object sender, EventArgs e)
        {

        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {

            

                if (txtDuracion.Text != "" && txtPrecio.Text != "")
                {

                    int precio = int.Parse(txtPrecio.Text);

                    int idPiloto1 = 0;
                    int idPiloto2 = 0;

                    int idServicio1 = 0;
                    int idServicio2 = 0;
                    int idServicio3 = 0;



                    if (validacionInsertar())
                    {

                        for (int x = 0; x < 2; x++)
                        {
                            if (x == 0)
                            {
                                cargarPiloto();
                                generarIdpiloto();
                                idPiloto1 = this.idPiloto;

                            }
                            else if (x == 1)
                            {
                                cargarPiloto();
                                generarIdpiloto();
                                idPiloto2 = this.idPiloto;


                            }
                        }


                        for (int x = 0; x < 3; x++)
                        {
                            if (x == 0)
                            {
                                cargarServicio();
                                generarIdservicio();
                                idServicio1 = this.idServicio;

                            }
                            else if (x == 1)
                            {
                                cargarServicio();
                                generarIdservicio();
                                idServicio2 = this.idServicio; ;

                            }
                            else if (x == 2)
                            {
                                cargarServicio();
                                generarIdservicio();
                                idServicio3 = this.idServicio;
                            }
                        }



                        int duracion = int.Parse(txtDuracion.Text);

                        string fechaSalida = (dtpFechasalida.Value.Date + dtpHorasalida.Value.TimeOfDay).ToString("dd/MM/yyyy hh:mm");

                        Console.WriteLine(fechaSalida);

                        string fechaLlegada = (dtpFechallegada.Value.Date + dtpHorallegada.Value.TimeOfDay).ToString("dd/MM/yyyy hh:mm");

                        Console.WriteLine(fechaLlegada);

                        new CNVuelo().insertarAerolinea(id_aerolinea, precio, fechaSalida, this.nombreAerosda, fechaLlegada, this.nombreAerolda, duracion, idPiloto1, idPiloto2, idServicio1, idServicio2, idServicio3, this.id_avion);

                        txtDuracion.Text = "";
                        txtPrecio.Text = "";




                    }
                    else
                    {
                        MessageBox.Show("No se puede efectuar ningun registro(No hay tripulantes disponibles)");
                    }
                }
                else
                {

                    MessageBox.Show("Rellene todos los campos");

                }
           

        }
        

        private void dgvAerolinea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.id_aerolinea = int.Parse(dgvAerolinea.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void dgvAvion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.id_avion= int.Parse(dgvAvion.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void dgvaeroSalida_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.nombreAerosda= dgvaeroSalida.Rows[e.RowIndex].Cells[2].Value.ToString();
            dataTablelda.DefaultView.RowFilter = $"Id_aeropuerto NOT LIKE '{dgvaeroSalida.Rows[e.RowIndex].Cells[0].Value.ToString()}%'";

        }

        private void dgvaeroLlegada_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.nombreAerolda= dgvaeroLlegada.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.frmMant.Visible = true;
            this.Hide();
        }
    }
}



