﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmVueloInteligente : Form
    {

        string fechaInicio;
        string fechaFinal;
        string aeropuertoO;
        string aeropuertoD;
        string fecha2;

        FrmPasajerorRegVuelo frm;

        DataTable dataPrueba;



        bool comIda = false;
        bool comVuelta = false;

        public FrmVueloInteligente(string fechaInicio, string fechaFinal, string aeropuertoO, string aeropuertoD)
        {
            InitializeComponent();

            this.fechaInicio = fechaInicio;
            this.fechaFinal = fechaFinal;
            this.aeropuertoO = aeropuertoO;
            this.aeropuertoD = aeropuertoD;




            definirdata();

            definirdata1();

            caragarVuelos();

            cargarfilas();

            Establecer();

            fechas();

        }


        public void cargarfilas()
        {
            ;

            dgvInteligente.Rows.Add();
            dgvInteligente.Rows.Add();
            dgvInteligente.Rows.Add();

            dgvInteligente.Rows.Add();
            dgvInteligente.Rows.Add();
            dgvInteligente.Rows.Add();

            dgvInteligente.Rows.Add();
            dgvInteligente.Rows.Add();

        }


        public void definirdata()
        {

            dataPrueba = new DataTable();

            dataPrueba.Columns.Add("Precio");
            dataPrueba.Columns.Add("Fecha_ini");
            dataPrueba.Columns.Add("Fecha_fin");

            dgvPrueba.DataSource = dataPrueba;


        }



        public void definirdata1()
        {

            DataTable Prueba1 = new DataTable();

           Prueba1.Columns.Add("Precio");
           Prueba1.Columns.Add("Fecha_ini");
           Prueba1.Columns.Add("Fecha_fin");

            dgvtop.DataSource =Prueba1;


        }


        public void caragarVuelos()
        {


   
            new CNVuelo().seleccionaVueloInteligen(dgvPrueba, this.aeropuertoO,this. aeropuertoD);

            DateTime fechaIni = Convert.ToDateTime(fechaInicio);


            DateTime fechaFin = Convert.ToDateTime(fechaFinal);

     
           

            dataPrueba.DefaultView.RowFilter = $"Fecha_ini LIKE '%{ fechaIni.ToString("dd/MM/yyyy")}%' and Fecha_fin LIKE '%{ fechaFin.ToString("dd/MM/yyyy")}%'";

        }
        


        public void Establecer(){

            DateTime fechaIni = Convert.ToDateTime(fechaInicio);


            DateTime fechaFin = Convert.ToDateTime(fechaFinal);


            DateTime fechaida = fechaIni.AddDays(-3);
            DateTime fechaVuelta = fechaFin.AddDays(-3);

            for (int x=1; x < 8; x++) {

                DateTime nfechaida = fechaida.AddDays(x-1);
                dgvInteligente.Rows[0].Cells[x].Value = nfechaida.ToString("dd/MM/yyyy");
            
            }

            for (int x = 1; x < 8; x++)
            {

                DateTime nfechaVuelta = fechaVuelta.AddDays(x-1);
                dgvInteligente.Rows[x].Cells[0].Value = nfechaVuelta.ToString("dd/MM/yyyy");

            }

        }


        public void fechas()
        {
            

            List<int> lista = new List<int>();

            DateTime fechaIni = Convert.ToDateTime(fechaInicio);


            DateTime fechaFin = Convert.ToDateTime(fechaFinal);





            int columana = 3;
            int fila = 3;

       
            
            for (int j = 0; j <= 3; j++)
            {

                for (int j1 = 0; j1 <= 3; j1++)
                {


                    DateTime fechaida = fechaIni.AddDays(j);
                    DateTime fechavuelta = fechaFin.AddDays(j1);

                  //  MessageBox.Show(fechaida.ToString("dd/MM/yyyy") + fechavuelta.ToString("dd/MM/yyyy"));

                    dataPrueba.DefaultView.RowFilter = $"Fecha_ini LIKE '%{fechaida.ToString("dd/MM/yyyy")}%' and Fecha_fin LIKE '%{fechavuelta.ToString("dd/MM/yyyy")}%'";
                   

                    int Rowcount = dgvPrueba.RowCount;
                    //MessageBox.Show("1---------------"  + (Rowcount-1).ToString());
                   // MessageBox.Show("2---------------"+ (dataPrueba.Rows.Count - 1).ToString());

                    if (Rowcount-1 > 0)
                    {

                    
                        for (int y = 0; y < Rowcount - 1; y++)
                        {


                            lista.Add(int.Parse(dgvPrueba.Rows[y].Cells[0].Value.ToString()));

                            //MessageBox.Show(dgvPrueba.Rows[y].Cells[0].Value.ToString());

                        }
                        int[] nlista = lista.OrderByDescending(i => i).ToArray();

                        dgvInteligente.Rows[4 + j1].Cells[4 + j].Value = nlista[nlista.Count() - 1];

                        lista.Clear();


                    }
               
        }

    }
            
            for (int j = 0; j <= 3; j++)
            {

                for (int j1 = 0; j1 <= 3; j1++)
                {


                    DateTime fechaida = fechaIni.AddDays(j);
                    DateTime fechavuelta = fechaFin.AddDays(-j1);

                   // MessageBox.Show(fechaida.ToString("dd/MM/yyy") + fechavuelta.ToString("dd/MM/yyy"));

                    dataPrueba.DefaultView.RowFilter = $"Fecha_ini LIKE '%{fechaida.ToString("dd/MM/yyyy")}%' and Fecha_fin LIKE '%{fechavuelta.ToString("dd/MM/yyyy")}%'";



                    int Rowcount = dgvPrueba.RowCount;

                    if (Rowcount-1 > 0)
                    {

                     

                        for (int y = 0; y < Rowcount-1; y++)
                        {




                            lista.Add(int.Parse(dgvPrueba.Rows[y].Cells[0].Value.ToString()));

                           // MessageBox.Show(dgvPrueba.Rows[y].Cells[0].Value.ToString());

                        }
                        int[] nlista = lista.OrderByDescending(i => i).ToArray();

                        dgvInteligente.Rows[4 - j1].Cells[4 + j].Value = nlista[nlista.Count() - 1];

                        lista.Clear();

                    }

                }
            }



            for (int j = 0; j <= 3; j++)
            {

                for (int j1 = 0; j1 <= 3; j1++)
                {

                    DateTime fechaida = fechaIni.AddDays(-j);
                    DateTime fechavuelta = fechaFin.AddDays(j1);

                    //MessageBox.Show(fechaida.ToString("dd/MM/yyy") + fechavuelta.ToString("dd/MM/yyy"));

                    dataPrueba.DefaultView.RowFilter = $"Fecha_ini LIKE '%{fechaida.ToString("dd/MM/yyyy")}%' and Fecha_fin LIKE '%{fechavuelta.ToString("dd/MM/yyyy")}%'";

                    int Rowcount = dgvPrueba.RowCount;

                    if (Rowcount-1 > 0)
                    {

                        //MessageBox.Show(dataPrueba.Rows.Count.ToString());

                        for (int y = 0; y < Rowcount - 1 ; y++)
                        {


                            lista.Add(int.Parse(dgvPrueba.Rows[y].Cells[0].Value.ToString()));

                            //MessageBox.Show(dgvPrueba.Rows[y].Cells[0].Value.ToString());

                        }
                        int[] nlista = lista.OrderByDescending(i => i).ToArray();

                        dgvInteligente.Rows[4 + j1].Cells[4 - j].Value = nlista[nlista.Count() - 1];

                        lista.Clear();

                    }
                }
            }

                for (int j = 0; j <= 3; j++)
                {

                    for (int j1 = 0; j1 <= 3; j1++)
                    {

                        DateTime fechaida = fechaIni.AddDays(-j);
                        DateTime fechavuelta = fechaFin.AddDays(-j1);

                       // MessageBox.Show(fechaida.ToString("dd/MM/yyy") + fechavuelta.ToString("dd/MM/yyy"));

                        dataPrueba.DefaultView.RowFilter = $"Fecha_ini LIKE '%{fechaida.ToString("dd/MM/yyyy")}%' and Fecha_fin LIKE '%{fechavuelta.ToString("dd/MM/yyyy")}%'";


                    int Rowcount = dgvPrueba.RowCount;

                    if (Rowcount-1 > 0)
                        {

                           // MessageBox.Show(dataPrueba.Rows.Count.ToString());

                            for (int y = 0; y < Rowcount - 1; y++)
                            {


                                lista.Add(int.Parse(dgvPrueba.Rows[y].Cells[0].Value.ToString()));

                                //MessageBox.Show(dgvPrueba.Rows[y].Cells[0].Value.ToString());

                            }
                            int[] nlista = lista.OrderByDescending(i => i).ToArray();

                            dgvInteligente.Rows[4 - j1].Cells[4 - j].Value = nlista[nlista.Count() - 1];

                            lista.Clear();

                        }
                    }

                }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }
    }

    }

     