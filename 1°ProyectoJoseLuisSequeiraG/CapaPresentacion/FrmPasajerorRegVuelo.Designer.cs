﻿namespace CapaPresentacion
{
    partial class FrmPasajerorRegVuelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvEscala1 = new System.Windows.Forms.DataGridView();
            this.Vuelos = new System.Windows.Forms.Label();
            this.panelEscala = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button22 = new System.Windows.Forms.Button();
            this.dgvEscala3 = new System.Windows.Forms.DataGridView();
            this.button23 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button21 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.btndgvEscala2 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnHidamayormenor = new System.Windows.Forms.Button();
            this.btnHidamenormayor = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAtras = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnprecioAscida = new System.Windows.Forms.Button();
            this.btnprecioDescida = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnConfirmarEscala = new System.Windows.Forms.Button();
            this.btnBuscarida = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpFechaida = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvEscala2 = new System.Windows.Forms.DataGridView();
            this.dgvVuelos = new System.Windows.Forms.DataGridView();
            this.panelMain = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpLlegada = new System.Windows.Forms.DateTimePicker();
            this.dtpSalida = new System.Windows.Forms.DateTimePicker();
            this.btnVuelo_Inteligente = new System.Windows.Forms.Button();
            this.dgvAeropuetoD = new System.Windows.Forms.DataGridView();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtAeroD = new System.Windows.Forms.TextBox();
            this.txtAeroO = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.dgvAeropuetoO = new System.Windows.Forms.DataGridView();
            this.ckbIdavuelta = new System.Windows.Forms.CheckBox();
            this.btndgvVuelos = new System.Windows.Forms.Button();
            this.btndgvEscala1 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala1)).BeginInit();
            this.panelEscala.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala3)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).BeginInit();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuetoD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuetoO)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aeropuero Origen";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(614, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Aeropuerto de destino";
            // 
            // dgvEscala1
            // 
            this.dgvEscala1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala1.Location = new System.Drawing.Point(21, 273);
            this.dgvEscala1.Name = "dgvEscala1";
            this.dgvEscala1.RowHeadersWidth = 51;
            this.dgvEscala1.RowTemplate.Height = 24;
            this.dgvEscala1.Size = new System.Drawing.Size(1022, 116);
            this.dgvEscala1.TabIndex = 4;
            this.dgvEscala1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEscala1_CellClick);
            // 
            // Vuelos
            // 
            this.Vuelos.AutoSize = true;
            this.Vuelos.Location = new System.Drawing.Point(23, 445);
            this.Vuelos.Name = "Vuelos";
            this.Vuelos.Size = new System.Drawing.Size(99, 17);
            this.Vuelos.TabIndex = 5;
            this.Vuelos.Text = "vuelo escala 2";
            // 
            // panelEscala
            // 
            this.panelEscala.Controls.Add(this.panel9);
            this.panelEscala.Controls.Add(this.button13);
            this.panelEscala.Controls.Add(this.btndgvEscala2);
            this.panelEscala.Controls.Add(this.dateTimePicker2);
            this.panelEscala.Controls.Add(this.label14);
            this.panelEscala.Controls.Add(this.button12);
            this.panelEscala.Controls.Add(this.dateTimePicker1);
            this.panelEscala.Controls.Add(this.label13);
            this.panelEscala.Controls.Add(this.panel5);
            this.panelEscala.Controls.Add(this.label20);
            this.panelEscala.Controls.Add(this.panel6);
            this.panelEscala.Controls.Add(this.panel4);
            this.panelEscala.Controls.Add(this.panel3);
            this.panelEscala.Controls.Add(this.panel2);
            this.panelEscala.Controls.Add(this.btnAtras);
            this.panelEscala.Controls.Add(this.panel1);
            this.panelEscala.Controls.Add(this.btnConfirmarEscala);
            this.panelEscala.Controls.Add(this.btnBuscarida);
            this.panelEscala.Controls.Add(this.label5);
            this.panelEscala.Controls.Add(this.dtpFechaida);
            this.panelEscala.Controls.Add(this.Vuelos);
            this.panelEscala.Controls.Add(this.label3);
            this.panelEscala.Controls.Add(this.dgvEscala2);
            this.panelEscala.Controls.Add(this.dgvEscala1);
            this.panelEscala.Controls.Add(this.dgvVuelos);
            this.panelEscala.Location = new System.Drawing.Point(9, 25);
            this.panelEscala.Name = "panelEscala";
            this.panelEscala.Size = new System.Drawing.Size(1252, 861);
            this.panelEscala.TabIndex = 7;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.button22);
            this.panel9.Controls.Add(this.dgvEscala3);
            this.panel9.Controls.Add(this.button23);
            this.panel9.Controls.Add(this.button1);
            this.panel9.Controls.Add(this.button20);
            this.panel9.Controls.Add(this.dateTimePicker4);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.button21);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Location = new System.Drawing.Point(11, 606);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1032, 190);
            this.panel9.TabIndex = 34;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(771, 29);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(122, 23);
            this.button22.TabIndex = 13;
            this.button22.Text = "Menor a mayor";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // dgvEscala3
            // 
            this.dgvEscala3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala3.Location = new System.Drawing.Point(3, 52);
            this.dgvEscala3.Name = "dgvEscala3";
            this.dgvEscala3.RowHeadersWidth = 51;
            this.dgvEscala3.RowTemplate.Height = 24;
            this.dgvEscala3.Size = new System.Drawing.Size(1024, 129);
            this.dgvEscala3.TabIndex = 32;
            this.dgvEscala3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEscala3_CellClick);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(899, 29);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(122, 23);
            this.button23.TabIndex = 14;
            this.button23.Text = "Mayor a menor";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(412, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(899, 3);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(122, 23);
            this.button20.TabIndex = 17;
            this.button20.Text = "Mayor a menor";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(206, 17);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker4.TabIndex = 36;
            this.dateTimePicker4.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(696, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 17);
            this.label18.TabIndex = 15;
            this.label18.Text = "precio:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(108, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 17);
            this.label16.TabIndex = 35;
            this.label16.Text = "Fecha salida:";
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(771, 3);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(122, 23);
            this.button21.TabIndex = 16;
            this.button21.Text = "Menor a mayor";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(696, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 17);
            this.label17.TabIndex = 18;
            this.label17.Text = "Duracion:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 17);
            this.label19.TabIndex = 31;
            this.label19.Text = "vuelo escala 2";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(435, 437);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 30;
            this.button13.Text = "Buscar";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // btndgvEscala2
            // 
            this.btndgvEscala2.Location = new System.Drawing.Point(1060, 511);
            this.btndgvEscala2.Name = "btndgvEscala2";
            this.btndgvEscala2.Size = new System.Drawing.Size(175, 23);
            this.btndgvEscala2.TabIndex = 12;
            this.btndgvEscala2.Text = "<---Vuelo inteligente";
            this.btndgvEscala2.UseVisualStyleBackColor = true;
            this.btndgvEscala2.Click += new System.EventHandler(this.btndgvEscala2_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(229, 438);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker2.TabIndex = 29;
            this.dateTimePicker2.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(131, 440);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "Fecha salida:";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(433, 239);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 27;
            this.button12.Text = "Buscar";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy ";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(227, 240);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 26;
            this.dateTimePicker1.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(129, 242);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 25;
            this.label13.Text = "Fecha salida:";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.button8);
            this.panel5.Controls.Add(this.button9);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Location = new System.Drawing.Point(667, 432);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(377, 25);
            this.panel5.TabIndex = 24;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(234, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(122, 23);
            this.button8.TabIndex = 17;
            this.button8.Text = "Mayor a menor";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(106, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(122, 23);
            this.button9.TabIndex = 16;
            this.button9.Text = "Menor a mayor";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Duracion:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 25);
            this.label20.TabIndex = 30;
            this.label20.Text = "Vuelos de ida:";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.button10);
            this.panel6.Controls.Add(this.button11);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Location = new System.Drawing.Point(667, 398);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(377, 28);
            this.panel6.TabIndex = 23;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(106, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(122, 23);
            this.button10.TabIndex = 13;
            this.button10.Text = "Menor a mayor";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(234, 3);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(122, 23);
            this.button11.TabIndex = 14;
            this.button11.Text = "Mayor a menor";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 17);
            this.label11.TabIndex = 15;
            this.label11.Text = "precio:";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.button6);
            this.panel4.Controls.Add(this.button7);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(667, 232);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(377, 25);
            this.panel4.TabIndex = 21;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(234, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(122, 23);
            this.button6.TabIndex = 17;
            this.button6.Text = "Mayor a menor";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(106, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(122, 23);
            this.button7.TabIndex = 16;
            this.button7.Text = "Menor a mayor";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "Duracion:";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(666, 198);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(377, 28);
            this.panel3.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(106, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(122, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Menor a mayor";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(234, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(122, 23);
            this.button5.TabIndex = 14;
            this.button5.Text = "Mayor a menor";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "precio:";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnHidamayormenor);
            this.panel2.Controls.Add(this.btnHidamenormayor);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(662, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(377, 25);
            this.panel2.TabIndex = 20;
            // 
            // btnHidamayormenor
            // 
            this.btnHidamayormenor.Location = new System.Drawing.Point(234, 2);
            this.btnHidamayormenor.Name = "btnHidamayormenor";
            this.btnHidamayormenor.Size = new System.Drawing.Size(122, 23);
            this.btnHidamayormenor.TabIndex = 17;
            this.btnHidamayormenor.Text = "Mayor a menor";
            this.btnHidamayormenor.UseVisualStyleBackColor = true;
            this.btnHidamayormenor.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnHidamenormayor
            // 
            this.btnHidamenormayor.Location = new System.Drawing.Point(106, 3);
            this.btnHidamenormayor.Name = "btnHidamenormayor";
            this.btnHidamenormayor.Size = new System.Drawing.Size(122, 23);
            this.btnHidamenormayor.TabIndex = 16;
            this.btnHidamenormayor.Text = "Menor a mayor";
            this.btnHidamenormayor.UseVisualStyleBackColor = true;
            this.btnHidamenormayor.Click += new System.EventHandler(this.btnHmenormayor_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Duracion:";
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(21, 830);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 9;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnprecioAscida);
            this.panel1.Controls.Add(this.btnprecioDescida);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(662, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(377, 28);
            this.panel1.TabIndex = 19;
            // 
            // btnprecioAscida
            // 
            this.btnprecioAscida.Location = new System.Drawing.Point(106, 3);
            this.btnprecioAscida.Name = "btnprecioAscida";
            this.btnprecioAscida.Size = new System.Drawing.Size(122, 23);
            this.btnprecioAscida.TabIndex = 13;
            this.btnprecioAscida.Text = "Menor a mayor";
            this.btnprecioAscida.UseVisualStyleBackColor = true;
            this.btnprecioAscida.Click += new System.EventHandler(this.btnAsc_Click);
            // 
            // btnprecioDescida
            // 
            this.btnprecioDescida.Location = new System.Drawing.Point(234, 3);
            this.btnprecioDescida.Name = "btnprecioDescida";
            this.btnprecioDescida.Size = new System.Drawing.Size(122, 23);
            this.btnprecioDescida.TabIndex = 14;
            this.btnprecioDescida.Text = "Mayor a menor";
            this.btnprecioDescida.UseVisualStyleBackColor = true;
            this.btnprecioDescida.Click += new System.EventHandler(this.button4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 15;
            this.label4.Text = "precio:";
            // 
            // btnConfirmarEscala
            // 
            this.btnConfirmarEscala.Location = new System.Drawing.Point(964, 830);
            this.btnConfirmarEscala.Name = "btnConfirmarEscala";
            this.btnConfirmarEscala.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmarEscala.TabIndex = 8;
            this.btnConfirmarEscala.Text = "Confirmar";
            this.btnConfirmarEscala.UseVisualStyleBackColor = true;
            this.btnConfirmarEscala.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnBuscarida
            // 
            this.btnBuscarida.Location = new System.Drawing.Point(537, 43);
            this.btnBuscarida.Name = "btnBuscarida";
            this.btnBuscarida.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarida.TabIndex = 12;
            this.btnBuscarida.Text = "Buscar";
            this.btnBuscarida.UseVisualStyleBackColor = true;
            this.btnBuscarida.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 240);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Vuelo escala 1";
            // 
            // dtpFechaida
            // 
            this.dtpFechaida.CustomFormat = "dd/MM/yyyy ";
            this.dtpFechaida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaida.Location = new System.Drawing.Point(331, 44);
            this.dtpFechaida.Name = "dtpFechaida";
            this.dtpFechaida.Size = new System.Drawing.Size(200, 22);
            this.dtpFechaida.TabIndex = 10;
            this.dtpFechaida.Value = new System.DateTime(2020, 8, 20, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(233, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Fecha salida:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // dgvEscala2
            // 
            this.dgvEscala2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEscala2.Location = new System.Drawing.Point(12, 464);
            this.dgvEscala2.Name = "dgvEscala2";
            this.dgvEscala2.RowHeadersWidth = 51;
            this.dgvEscala2.RowTemplate.Height = 24;
            this.dgvEscala2.Size = new System.Drawing.Size(1032, 122);
            this.dgvEscala2.TabIndex = 5;
            this.dgvEscala2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEscala2_CellClick);
            // 
            // dgvVuelos
            // 
            this.dgvVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVuelos.Location = new System.Drawing.Point(21, 73);
            this.dgvVuelos.Name = "dgvVuelos";
            this.dgvVuelos.RowHeadersWidth = 51;
            this.dgvVuelos.RowTemplate.Height = 24;
            this.dgvVuelos.Size = new System.Drawing.Size(1021, 116);
            this.dgvVuelos.TabIndex = 35;
            this.dgvVuelos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVuelos_CellClick);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.label10);
            this.panelMain.Controls.Add(this.label6);
            this.panelMain.Controls.Add(this.dtpLlegada);
            this.panelMain.Controls.Add(this.dtpSalida);
            this.panelMain.Controls.Add(this.btnVuelo_Inteligente);
            this.panelMain.Controls.Add(this.dgvAeropuetoD);
            this.panelMain.Controls.Add(this.label24);
            this.panelMain.Controls.Add(this.label23);
            this.panelMain.Controls.Add(this.txtAeroD);
            this.panelMain.Controls.Add(this.txtAeroO);
            this.panelMain.Controls.Add(this.label22);
            this.panelMain.Controls.Add(this.dgvAeropuetoO);
            this.panelMain.Controls.Add(this.ckbIdavuelta);
            this.panelMain.Controls.Add(this.label2);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Location = new System.Drawing.Point(16, 12);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1068, 506);
            this.panelMain.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(59, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 17);
            this.label10.TabIndex = 42;
            this.label10.Text = "Fecha ida:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(381, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "Fecha vuelta:";
            // 
            // dtpLlegada
            // 
            this.dtpLlegada.CustomFormat = "dd/MM/yyyy";
            this.dtpLlegada.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLlegada.Location = new System.Drawing.Point(480, 105);
            this.dtpLlegada.Name = "dtpLlegada";
            this.dtpLlegada.Size = new System.Drawing.Size(200, 22);
            this.dtpLlegada.TabIndex = 40;
            // 
            // dtpSalida
            // 
            this.dtpSalida.CustomFormat = "dd/MM/yyyy";
            this.dtpSalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSalida.Location = new System.Drawing.Point(145, 105);
            this.dtpSalida.Name = "dtpSalida";
            this.dtpSalida.Size = new System.Drawing.Size(200, 22);
            this.dtpSalida.TabIndex = 39;
            // 
            // btnVuelo_Inteligente
            // 
            this.btnVuelo_Inteligente.Location = new System.Drawing.Point(40, 463);
            this.btnVuelo_Inteligente.Name = "btnVuelo_Inteligente";
            this.btnVuelo_Inteligente.Size = new System.Drawing.Size(75, 23);
            this.btnVuelo_Inteligente.TabIndex = 38;
            this.btnVuelo_Inteligente.Text = "Atras";
            this.btnVuelo_Inteligente.UseVisualStyleBackColor = true;
            this.btnVuelo_Inteligente.Click += new System.EventHandler(this.btnVuelo_Inteligente_Click);
            // 
            // dgvAeropuetoD
            // 
            this.dgvAeropuetoD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAeropuetoD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAeropuetoD.Location = new System.Drawing.Point(549, 225);
            this.dgvAeropuetoD.Name = "dgvAeropuetoD";
            this.dgvAeropuetoD.ReadOnly = true;
            this.dgvAeropuetoD.RowHeadersWidth = 51;
            this.dgvAeropuetoD.RowTemplate.Height = 24;
            this.dgvAeropuetoD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAeropuetoD.Size = new System.Drawing.Size(481, 221);
            this.dgvAeropuetoD.TabIndex = 37;
            this.dgvAeropuetoD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAeropuetoD_CellClick_1);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(555, 205);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 17);
            this.label24.TabIndex = 36;
            this.label24.Text = "Buscar:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(42, 203);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 17);
            this.label23.TabIndex = 35;
            this.label23.Text = "Buscar:";
            // 
            // txtAeroD
            // 
            this.txtAeroD.Location = new System.Drawing.Point(617, 200);
            this.txtAeroD.Name = "txtAeroD";
            this.txtAeroD.Size = new System.Drawing.Size(263, 22);
            this.txtAeroD.TabIndex = 34;
            this.txtAeroD.TextChanged += new System.EventHandler(this.txtAeroD_TextChanged);
            // 
            // txtAeroO
            // 
            this.txtAeroO.Location = new System.Drawing.Point(104, 200);
            this.txtAeroO.Name = "txtAeroO";
            this.txtAeroO.Size = new System.Drawing.Size(272, 22);
            this.txtAeroO.TabIndex = 33;
            this.txtAeroO.TextChanged += new System.EventHandler(this.txtAeroO_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(414, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(214, 29);
            this.label22.TabIndex = 32;
            this.label22.Text = "Registro de vuelos";
            // 
            // dgvAeropuetoO
            // 
            this.dgvAeropuetoO.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAeropuetoO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAeropuetoO.Location = new System.Drawing.Point(38, 228);
            this.dgvAeropuetoO.Name = "dgvAeropuetoO";
            this.dgvAeropuetoO.ReadOnly = true;
            this.dgvAeropuetoO.RowHeadersWidth = 51;
            this.dgvAeropuetoO.RowTemplate.Height = 24;
            this.dgvAeropuetoO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAeropuetoO.Size = new System.Drawing.Size(474, 218);
            this.dgvAeropuetoO.TabIndex = 29;
            this.dgvAeropuetoO.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAeropuetoO_CellClick_1);
            // 
            // ckbIdavuelta
            // 
            this.ckbIdavuelta.AutoSize = true;
            this.ckbIdavuelta.Checked = true;
            this.ckbIdavuelta.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbIdavuelta.Location = new System.Drawing.Point(41, 50);
            this.ckbIdavuelta.Name = "ckbIdavuelta";
            this.ckbIdavuelta.Size = new System.Drawing.Size(102, 21);
            this.ckbIdavuelta.TabIndex = 28;
            this.ckbIdavuelta.Text = "Ida y vuelta";
            this.ckbIdavuelta.UseVisualStyleBackColor = true;
            // 
            // btndgvVuelos
            // 
            this.btndgvVuelos.Location = new System.Drawing.Point(1057, 134);
            this.btndgvVuelos.Name = "btndgvVuelos";
            this.btndgvVuelos.Size = new System.Drawing.Size(177, 23);
            this.btndgvVuelos.TabIndex = 10;
            this.btndgvVuelos.Text = "<---Vuelo inteligente";
            this.btndgvVuelos.UseVisualStyleBackColor = true;
            this.btndgvVuelos.Click += new System.EventHandler(this.btndgvVuelos_Click);
            // 
            // btndgvEscala1
            // 
            this.btndgvEscala1.Location = new System.Drawing.Point(1059, 342);
            this.btndgvEscala1.Name = "btndgvEscala1";
            this.btndgvEscala1.Size = new System.Drawing.Size(175, 23);
            this.btndgvEscala1.TabIndex = 11;
            this.btndgvEscala1.Text = "<---Vuelo inteligente";
            this.btndgvEscala1.UseVisualStyleBackColor = true;
            this.btndgvEscala1.Click += new System.EventHandler(this.btndgvEscala1_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(1059, 729);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(185, 23);
            this.button15.TabIndex = 13;
            this.button15.Text = "<---Vuelo inteligente";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // FrmPasajerorRegVuelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1273, 889);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.btndgvEscala1);
            this.Controls.Add(this.btndgvVuelos);
            this.Controls.Add(this.panelEscala);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPasajerorRegVuelo";
            this.Text = "FrmMenuPasajero";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala1)).EndInit();
            this.panelEscala.ResumeLayout(false);
            this.panelEscala.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala3)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEscala2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuetoD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuetoO)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvEscala1;
        private System.Windows.Forms.Label Vuelos;
        private System.Windows.Forms.Panel panelEscala;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.DateTimePicker dtpFechaida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvVuelos;
        private System.Windows.Forms.DataGridView dgvEscala2;
        private System.Windows.Forms.Button btnBuscarida;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnprecioDescida;
        private System.Windows.Forms.Button btnprecioAscida;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnConfirmarEscala;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnHidamayormenor;
        private System.Windows.Forms.Button btnHidamenormayor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox ckbIdavuelta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dgvEscala3;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView dgvAeropuetoO;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtAeroD;
        private System.Windows.Forms.TextBox txtAeroO;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView dgvAeropuetoD;
        private System.Windows.Forms.Button btnVuelo_Inteligente;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpLlegada;
        private System.Windows.Forms.DateTimePicker dtpSalida;
        private System.Windows.Forms.Button btndgvVuelos;
        private System.Windows.Forms.Button btndgvEscala1;
        private System.Windows.Forms.Button btndgvEscala2;
        private System.Windows.Forms.Button button15;
    }
}