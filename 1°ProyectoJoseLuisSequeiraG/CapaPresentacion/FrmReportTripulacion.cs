﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class FrmReportTripulacion : Form
    {
        FrmReportMenu menu;
        public FrmReportTripulacion(FrmReportMenu menu)
        {
            InitializeComponent();
            CenterToParent();
            this.menu = menu;
            definirdgvAero();
            cargarAero();
        

        }

        public void definirdgvAero()
        {

            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id_aerolinea");
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Año");
            dataTable.Columns.Add("Tipo");

            dgvAerolinea.DataSource = dataTable;
        }
        public void definirdgvTripulante()
        {

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Nombre");
            dataTable.Columns.Add("Cedula");
            dataTable.Columns.Add("Fecha de nacimiento");
            dataTable.Columns.Add("Rol");
            dataTable.Columns.Add("Estado");
            dataTable.Columns.Add("Aerolinea");
            dgvTripulacion.DataSource = dataTable;


        }

        public void cargarAero() {

            new CNAerolinea().seleccionarAerolineas(dgvAerolinea);
        
        }

        private void dgvAerolinea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvTripulacion.Columns.Clear();
            definirdgvTripulante();

            new CNTripulacion().cagarTripulacionAerolinea(dgvTripulacion, dgvAerolinea.Rows[e.RowIndex].Cells[1].Value.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            menu.Visible = true;
            this.Hide();
        }
    }
}
