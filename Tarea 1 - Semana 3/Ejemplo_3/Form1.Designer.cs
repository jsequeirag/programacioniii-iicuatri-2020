﻿namespace Ejemplo_3
{
    partial class cbxNot3
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnEnvio1 = new System.Windows.Forms.RadioButton();
            this.rbtnEnvio2 = new System.Windows.Forms.RadioButton();
            this.rbtnEnvio3 = new System.Windows.Forms.RadioButton();
            this.lblMsj = new System.Windows.Forms.Label();
            this.lblMsj2 = new System.Windows.Forms.Label();
            this.chkNot1 = new System.Windows.Forms.CheckBox();
            this.chkNot2 = new System.Windows.Forms.CheckBox();
            this.chkNot3 = new System.Windows.Forms.CheckBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // rbtnEnvio1
            // 
            this.rbtnEnvio1.AutoSize = true;
            this.rbtnEnvio1.Location = new System.Drawing.Point(72, 119);
            this.rbtnEnvio1.Name = "rbtnEnvio1";
            this.rbtnEnvio1.Size = new System.Drawing.Size(17, 16);
            this.rbtnEnvio1.TabIndex = 0;
            this.rbtnEnvio1.TabStop = true;
            this.rbtnEnvio1.UseVisualStyleBackColor = true;
            // 
            // rbtnEnvio2
            // 
            this.rbtnEnvio2.AutoSize = true;
            this.rbtnEnvio2.Location = new System.Drawing.Point(73, 167);
            this.rbtnEnvio2.Name = "rbtnEnvio2";
            this.rbtnEnvio2.Size = new System.Drawing.Size(17, 16);
            this.rbtnEnvio2.TabIndex = 1;
            this.rbtnEnvio2.TabStop = true;
            this.rbtnEnvio2.UseVisualStyleBackColor = true;
            // 
            // rbtnEnvio3
            // 
            this.rbtnEnvio3.AutoSize = true;
            this.rbtnEnvio3.Location = new System.Drawing.Point(73, 220);
            this.rbtnEnvio3.Name = "rbtnEnvio3";
            this.rbtnEnvio3.Size = new System.Drawing.Size(17, 16);
            this.rbtnEnvio3.TabIndex = 2;
            this.rbtnEnvio3.TabStop = true;
            this.rbtnEnvio3.UseVisualStyleBackColor = true;
            // 
            // lblMsj
            // 
            this.lblMsj.AutoSize = true;
            this.lblMsj.Location = new System.Drawing.Point(38, 57);
            this.lblMsj.Name = "lblMsj";
            this.lblMsj.Size = new System.Drawing.Size(51, 20);
            this.lblMsj.TabIndex = 3;
            this.lblMsj.Text = "_______";
            this.lblMsj.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblMsj2
            // 
            this.lblMsj2.AutoSize = true;
            this.lblMsj2.Location = new System.Drawing.Point(39, 276);
            this.lblMsj2.Name = "lblMsj2";
            this.lblMsj2.Size = new System.Drawing.Size(51, 20);
            this.lblMsj2.TabIndex = 4;
            this.lblMsj2.Text = "_______";
            this.lblMsj2.Click += new System.EventHandler(this.lblAnunciado2_Click);
            // 
            // chkNot1
            // 
            this.chkNot1.AutoSize = true;
            this.chkNot1.Location = new System.Drawing.Point(73, 314);
            this.chkNot1.Name = "chkNot1";
            this.chkNot1.Size = new System.Drawing.Size(18, 17);
            this.chkNot1.TabIndex = 5;
            this.chkNot1.UseVisualStyleBackColor = true;
            // 
            // chkNot2
            // 
            this.chkNot2.AutoSize = true;
            this.chkNot2.Location = new System.Drawing.Point(73, 345);
            this.chkNot2.Name = "chkNot2";
            this.chkNot2.Size = new System.Drawing.Size(18, 17);
            this.chkNot2.TabIndex = 6;
            this.chkNot2.UseVisualStyleBackColor = true;
            // 
            // chkNot3
            // 
            this.chkNot3.AutoSize = true;
            this.chkNot3.Location = new System.Drawing.Point(73, 375);
            this.chkNot3.Name = "chkNot3";
            this.chkNot3.Size = new System.Drawing.Size(18, 17);
            this.chkNot3.TabIndex = 7;
            this.chkNot3.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(405, 57);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(157, 29);
            this.btnCalcular.TabIndex = 8;
            this.btnCalcular.Text = "Calcular gastos";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(405, 92);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(157, 27);
            this.txtResultado.TabIndex = 9;
            this.txtResultado.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // cbxNot3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.chkNot3);
            this.Controls.Add(this.chkNot2);
            this.Controls.Add(this.chkNot1);
            this.Controls.Add(this.lblMsj2);
            this.Controls.Add(this.lblMsj);
            this.Controls.Add(this.rbtnEnvio3);
            this.Controls.Add(this.rbtnEnvio2);
            this.Controls.Add(this.rbtnEnvio1);
            this.Name = "cbxNot3";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnEnvio1;
        private System.Windows.Forms.RadioButton rbtnEnvio2;
        private System.Windows.Forms.RadioButton rbtnEnvio3;
        private System.Windows.Forms.Label lblMsj;
        private System.Windows.Forms.Label lblMsj2;
        private System.Windows.Forms.CheckBox chkNot1;
        private System.Windows.Forms.CheckBox chkNot2;
        private System.Windows.Forms.CheckBox chkNot3;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtResultado;
    }
}

