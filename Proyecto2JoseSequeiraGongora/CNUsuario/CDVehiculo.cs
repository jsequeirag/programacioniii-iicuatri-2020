﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace CapaDatos
{
   public class CDVehiculo
    {

        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent. FullName, "gestion.xml");

        public void cargarPlacas(String cedula,ComboBox cbPlacas, Dictionary<string, string> placaTipo, Dictionary<string, string> placaAnnio)
        {
           
            doc.Load(ruta);



            XmlNodeList vehiculos = doc.SelectNodes("gestion/vehiculos/vehiculo");

            XmlNode unVehiculo;

            for (int i = 0; i < vehiculos.Count; i++)
            {


                unVehiculo = vehiculos.Item(i);

                string duenno= unVehiculo.SelectSingleNode("duenno").InnerText;

                if (cedula == duenno) {

                string placa = unVehiculo.SelectSingleNode("placa").InnerText;

                string tipo = unVehiculo.SelectSingleNode("tipo").InnerText;

                string annio= unVehiculo.SelectSingleNode("annio").InnerText;



                cbPlacas.Items.Add(placa);
                placaTipo[placa] = tipo;
                placaAnnio[placa]=annio;

                }
            }


            }

    
        }

    }
