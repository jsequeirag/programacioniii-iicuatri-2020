﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;

namespace Proyecto2JoseSequeiraGongora
{
   public class xmlDoc
    {
        List<int> precioMenores;
        List<int> todosPrecios;

        int numeromenor = 0;

        XmlDocument doc = new XmlDocument();

        string ruta = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName, "Menu.xml");

        public void LeerXml()
        {
            doc.Load(ruta);


            XmlNodeList listaPlatillos = doc.SelectNodes("gestion/Usuario/Usu");

            XmlNode unPlatillo;

            for (int i = 0; i < listaPlatillos.Count; i++)
            {
                unPlatillo = listaPlatillos.Item(i);

                string nombre = unPlatillo.SelectSingleNode("Nombre").InnerText;

                string apellido = unPlatillo.SelectSingleNode("Apellido").InnerText;

                Console.WriteLine(nombre+apellido);

            }
        }
    }
}
