﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Usuario
    {
        string nombre;
        string cedula;
        string contrasenna;
        string tipo;
        string id;

        public Usuario(string nombre, string cedula, string contrasenna, string tipo)
        {
            this.Nombre = nombre;
            this.Cedula = cedula;
            this.Contrasenna = contrasenna;
            this.Tipo = tipo;
        }

        public Usuario()
        {
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public string Contrasenna { get => contrasenna; set => contrasenna = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Id { get => id; set => id = value; }
    }
}
