﻿namespace Proyecto2JoseSequeiraGongora
{
    partial class FrmEventoOfiJuzgado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEventos = new System.Windows.Forms.DataGridView();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnTramitar = new System.Windows.Forms.Button();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblCedula = new System.Windows.Forms.Label();
            this.btnReportes = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventos)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvEventos
            // 
            this.dgvEventos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvEventos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEventos.Location = new System.Drawing.Point(5, 56);
            this.dgvEventos.Name = "dgvEventos";
            this.dgvEventos.ReadOnly = true;
            this.dgvEventos.RowHeadersWidth = 51;
            this.dgvEventos.RowTemplate.Height = 24;
            this.dgvEventos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEventos.Size = new System.Drawing.Size(505, 176);
            this.dgvEventos.TabIndex = 0;
            this.dgvEventos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEventos_CellClick);
            // 
            // btnAtras
            // 
            this.btnAtras.BackgroundImage = global::Proyecto2JoseSequeiraGongora.Properties.Resources.backgound2;
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAtras.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAtras.Location = new System.Drawing.Point(5, 238);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(92, 32);
            this.btnAtras.TabIndex = 1;
            this.btnAtras.Text = "Atras";
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnTramitar
            // 
            this.btnTramitar.BackgroundImage = global::Proyecto2JoseSequeiraGongora.Properties.Resources.backgound2;
            this.btnTramitar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTramitar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnTramitar.Location = new System.Drawing.Point(418, 238);
            this.btnTramitar.Name = "btnTramitar";
            this.btnTramitar.Size = new System.Drawing.Size(92, 32);
            this.btnTramitar.TabIndex = 2;
            this.btnTramitar.Text = "Tramitar";
            this.btnTramitar.UseVisualStyleBackColor = true;
            this.btnTramitar.Click += new System.EventHandler(this.btnTramitar_Click);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.BackColor = System.Drawing.Color.Transparent;
            this.lblNombre.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNombre.Location = new System.Drawing.Point(3, 11);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(46, 17);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "label1";
            // 
            // lblCedula
            // 
            this.lblCedula.AutoSize = true;
            this.lblCedula.BackColor = System.Drawing.Color.Transparent;
            this.lblCedula.ForeColor = System.Drawing.SystemColors.Control;
            this.lblCedula.Location = new System.Drawing.Point(3, 28);
            this.lblCedula.Name = "lblCedula";
            this.lblCedula.Size = new System.Drawing.Size(46, 17);
            this.lblCedula.TabIndex = 4;
            this.lblCedula.Text = "label2";
            // 
            // btnReportes
            // 
            this.btnReportes.BackgroundImage = global::Proyecto2JoseSequeiraGongora.Properties.Resources.backgound2;
            this.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReportes.ForeColor = System.Drawing.SystemColors.Control;
            this.btnReportes.Location = new System.Drawing.Point(312, 238);
            this.btnReportes.Name = "btnReportes";
            this.btnReportes.Size = new System.Drawing.Size(92, 32);
            this.btnReportes.TabIndex = 5;
            this.btnReportes.Text = "Reporte";
            this.btnReportes.UseVisualStyleBackColor = true;
            this.btnReportes.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(225, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Eventos";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Proyecto2JoseSequeiraGongora.Properties.Resources.backgound2;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblNombre);
            this.panel1.Controls.Add(this.lblCedula);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 50);
            this.panel1.TabIndex = 7;
            // 
            // FrmEventoOfiJuzgado
            // 
            this.BackgroundImage = global::Proyecto2JoseSequeiraGongora.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(514, 276);
            this.Controls.Add(this.btnReportes);
            this.Controls.Add(this.btnTramitar);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.dgvEventos);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmEventoOfiJuzgado";
            this.Text = "FrmEventoOfiJuzgado";
            this.Load += new System.EventHandler(this.FrmEventoOfiJuzgado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEventos;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnTramitar;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblCedula;
        private System.Windows.Forms.Button btnReportes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
    }
}