﻿using CapaNegocio;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Proyecto2JoseSequeiraGongora
{



    public partial class FrmEventoOfiJuzgado : Form
    {
        Usuario usuario;
        string codigo;
        
        List<string> listaReportes;


        public FrmEventoOfiJuzgado(Usuario usuario)
        {
            InitializeComponent();
            CenterToScreen();
            this.usuario = usuario;
            definirDgv();
            cargarEventos();
            lblNombre.Text = usuario.Nombre;
            lblCedula.Text = usuario.Cedula;
            listaReportes = new List<string>();
            establecerFilas();
        }
        public void definirDgv()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Placa");
            dt.Columns.Add("Lugar");
            dt.Columns.Add("Fecha");
            dt.Columns.Add("Multa");
            dgvEventos.DataSource = dt;
   
        }

        public void cargarEventos()
        {

            new CNEventos().cargarSoloeventos(2, dgvEventos);

        }

        public void establecerFilas()
        {

            dgvEventos.DefaultCellStyle.SelectionBackColor = Color.White;
            dgvEventos.DefaultCellStyle.SelectionForeColor = Color.Black;

        }

        private void dgvEventos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {

                dgvEventos.DefaultCellStyle.SelectionBackColor = Color.BlueViolet;
                dgvEventos.DefaultCellStyle.SelectionForeColor = Color.White;
                this.codigo = dgvEventos.Rows[e.RowIndex].Cells[0].Value.ToString();

            }
        }

        private void btnTramitar_Click(object sender, EventArgs e)
        {
            string hoy = DateTime.Now.ToLocalTime().ToString();

            new CNEventos().cargarparteOregistro(2, listaReportes);

            int numeroreporte = new Random().Next(0, 10000);

            while (listaReportes.Contains(codigo.ToString()))
            {

                numeroreporte = new Random().Next(0, 10000);

                MessageBox.Show("Número Repetido" + codigo);
            }

            new CNEventos().modificarEvento(2, this.codigo, this.usuario.Cedula, numeroreporte.ToString(),hoy);
            dgvEventos.Columns.Clear();
            listaReportes.Clear();
            definirDgv();
            cargarEventos();
            establecerFilas();
        }

        private void FrmEventoOfiJuzgado_Load(object sender, EventArgs e)
        {
            FrmMain frm = new FrmMain();
            frm.Visible = true;
            this.Hide();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            FrmReportes frm = new FrmReportes(this.usuario);
            frm.Visible = true;
            this.Hide();

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmMain frm = new FrmMain();
            frm.Visible = true;
            this.Hide();
        }
    }
}
