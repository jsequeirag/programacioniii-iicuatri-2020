﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using Entidades;

namespace Proyecto2JoseSequeiraGongora
{
    public partial class FrmMain : Form
    {

        Usuario usuario;
        public FrmMain()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void btnAcceder_Click(object sender, EventArgs e)
        {
            string iniUsuario = txtUsuario.Text;
            string iniCcontrasenna = txtcontrasenna.Text;

           this.usuario= new CNUsuario().loginUsuario(this.usuario,iniUsuario,iniCcontrasenna);

            if (this.usuario.Nombre == null)
            {

                MessageBox.Show("Usuario no registrado");
            }
            else {
               
                if (this.usuario.Tipo == "Ciudadano") {
                    FrmEventoCiudano frm = new FrmEventoCiudano(this.usuario);
                    frm.Visible = true;
                    this.Hide();
                }

                if (this.usuario.Tipo == "Oficial de Tránsito")
                {
                    FrmEventoOfiTransito frm = new FrmEventoOfiTransito(this.usuario);
                    frm.Visible = true;
                    this.Hide();
                }
                if (this.usuario.Tipo == "Oficina del Juzgado")
                {
                    FrmEventoOfiJuzgado frm = new FrmEventoOfiJuzgado(this.usuario);
                    frm.Visible = true;
                    this.Hide();
                }
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }
    }
}
