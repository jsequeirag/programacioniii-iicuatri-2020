﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class Insertar : Form
    {
        Principal principal;
        public Insertar(Principal principal)
        {
            this.principal = principal;

            InitializeComponent();
            this.CenterToScreen();

            cargarDepartamentos();
            cargarPuestos();
          
            cbIngles.Items.Add("A1");
            cbIngles.Items.Add("A2");
            cbIngles.Items.Add("B1");
            cbIngles.Items.Add("B2");
            cbIngles.Items.Add("C1");
            cbGenero.Items.Add('M');
            cbGenero.Items.Add('F');

            cbDepartamento.SelectedIndex=0;
            cbPuesto.SelectedIndex = 0;
            cbIngles.SelectedIndex = 0;
            cbGenero.SelectedIndex = 0;

  


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }



        public void cargarDepartamentos()
        {

            List<string> departamentos = new Ndepartamento().Obtenerdepartamentos();

            for (int x = 0; x < departamentos.Count(); x++)
            {
                cbDepartamento.Items.Add(departamentos[x]);
            }
        }



        public void cargarPuestos()
        {

            List<string> puestos = new Npuesto().ObtenerPuesto();

            for (int x = 0; x < puestos.Count(); x++)
            {
                cbPuesto.Items.Add(puestos[x]);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cedula = txtCedula.Text;
            string nombre = txtNombre.Text;
            char genero =char.Parse(cbGenero.Text);

         
            string idDepartamento = "";

            if (cbDepartamento.SelectedItem.Equals("Produccion"))
            {
                idDepartamento = "pr";

            }
            if (cbDepartamento.SelectedItem.Equals("Aseguramiento de la calidad"))
            {
                idDepartamento = "as";

            }
            if (cbDepartamento.SelectedItem.Equals("Data entry"))
            {
                idDepartamento = "de";
                    
            }

         
        
            string fechaNacimiento = dtpfechaNacimiento.Value.ToString();




            int edad = int.Parse(txtEdad.Text);
            string fechaIngreso = dtpfechaIngreso.Value.ToString();
            string Ningles = cbIngles.Text;

            int idPuesto=0;

            if (cbPuesto.SelectedItem.Equals("Ingeniero de Software I"))
            {
                idPuesto = 1;

            }
            if (cbPuesto.SelectedItem.Equals("Ingeniero de Software II"))
            {
                idPuesto = 2;

            }
            if (cbPuesto.SelectedItem.Equals("Senior"))
            {
                idPuesto = 3;

            }
            if (cbPuesto.SelectedItem.Equals("Ingeniero de calidad"))
            {
                idPuesto = 4;

            }
            if (cbPuesto.SelectedItem.Equals("Arquitecto"))
            {
                idPuesto = 5;

            }


            new Ncolaboradores().insertaColaborador(cedula, nombre, genero, idDepartamento, fechaNacimiento, edad, fechaIngreso, Ningles, idPuesto);


        }

        private void dtpfechaIngreso_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            principal.Show();
        }
    }

}
