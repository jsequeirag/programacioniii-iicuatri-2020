﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class ConEdad : Form
    {
        Principal principal;
        public ConEdad(Principal principal)
        {
            InitializeComponent();
            this.principal = principal;
            this.CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
            List<string> colaboradores = new Ncolaboradores().obtenerColaboradoredad(int.Parse(textBox1.Text), int.Parse(textBox2.Text));
            listView1.Items.Clear();
            for (int x = 0; x < colaboradores.Count(); x++)
            {

                listView1.Items.Add(colaboradores[x]);

            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            principal.Show();
        }
    }
}