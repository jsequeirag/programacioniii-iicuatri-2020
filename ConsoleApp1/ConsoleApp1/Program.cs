﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int x = 9;

            //Formas de concatenar

            Console.WriteLine($"Hola{x}");

            Console.WriteLine("Hola{0}",x);

            //sobrecarga

            int x1 = 1;
            int y = 2;
            int z = 3;

            Console.WriteLine(suma(x1, y,z));

            Console.WriteLine(suma(x1, y));
        }



        //Sobre Carga y lambda

        static int suma(int x, int y) => x + y;


        static int suma(int x, int y,int z) =>  x + y+z;


    }
}
